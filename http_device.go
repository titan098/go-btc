package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"sync"

	"bitbucket.org/titan098/go-btc/bip32"
	"bitbucket.org/titan098/go-btc/devices"

	"github.com/gorilla/mux"
)

// DeviceHandler contains the functions which respond to the /device
// url paths.
type DeviceHandler struct {
	devices []*devices.TrezorDevice

	waitingMutex    *sync.Mutex
	pinChan         map[string]chan string
	responseChan    map[string]chan interface{}
	waitingResponse []chan bool
}

type pinRequest struct {
	Pin string `json:"pin"`
}

type GetAddress struct {
	Coin   string   `json:"coin"`
	Path   []uint32 `json:"path"`
	Show   bool     `json:"show"`
	Cancel bool     `json:"cancel"`
}

// PinRequired represents a response from the device where a pin
// is required.
type PinRequired struct {
	PinRequired bool `json:"pinRequired"`
}

// List will list the available KeepKey devices
// GET: /device/list
// Response: A dictionary of devices keyed by deviceid
func (d *DeviceHandler) List(w http.ResponseWriter, r *http.Request) {
	deviceList := map[string]interface{}{}
	for _, device := range d.devices {
		deviceList[device.DeviceID] = device.Features
	}
	sendResponse(w, deviceList)
}

// Entropy will request entropy from the device.
// GET: /device/entropy/{deviceid}/{size}
// Response: Base64 encoded entropy from the device
func (d *DeviceHandler) Entropy(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	size, err := strconv.Atoi(vars["size"])
	if err != nil {
		sendError(w, errors.New("size must be an integer"), http.StatusBadRequest)
		return
	}

	entropy, err := device.GetEntropy(uint32(size))
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}
	sendResponse(w, entropy)
}

// GetAddress return the base58 encoded address for a given path
// POST: /device/getaddress/{deviceid}
// Response: base58 encoded address from the path
func (d *DeviceHandler) GetAddress(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	var addressRequest GetAddress
	err := json.NewDecoder(r.Body).Decode(&addressRequest)
	if err != nil {
		sendError(w, errors.New("invalid address request"), http.StatusBadRequest)
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	requestChan := make(chan PinRequired)
	pinRequest := func(needed bool) {
		pinChannel := d.pinChan[device.DeviceID]
		if pinChannel == nil {
			if needed {
				d.pinChan[device.DeviceID] = make(chan string)
				requestChan <- PinRequired{PinRequired: true}
			} else {
				requestChan <- PinRequired{PinRequired: false}
			}
		}
	}

	go device.GetAddress(addressRequest.Path, addressRequest.Coin, addressRequest.Show, pinRequest, d.responseToChanForDevice(device.DeviceID))
	response := <-requestChan
	sendResponse(w, response)
}

// Cancel will request the device to cancel the current workflow
// GET: /device/cancel/{deviceid}
func (d *DeviceHandler) Cancel(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	err := device.Cancel()
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	// cancel the pending channels so that the go routines don't
	// sit waiting for a response
	pinChannel := d.pinChan[device.DeviceID]
	if pinChannel != nil {
		log.Info("device was waiting for a pin... cancelling")
		close(pinChannel)
		d.pinChan[device.DeviceID] = nil
	}

	sendResponse(w, "action cancelled")
}

// VerifyMessage verifies a coin message from the device
// POST: /device/verifymessage/{deviceid}
// Input: {"address": "<String>", "signature": <b64data>, "message": <b64data>, "coin": <string>}
func (d *DeviceHandler) VerifyMessage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	var signature devices.Signature
	err := json.NewDecoder(r.Body).Decode(&signature)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	networkConstants := bip32.NetworkType(signature.Coin)
	if networkConstants.Name == "None" {
		sendError(w, errors.New("Invalid coin type"), http.StatusBadRequest)
		return
	}

	verified, err := device.VerifyMessage(signature.Address, signature.Signature, signature.Message, networkConstants)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}
	sendResponse(w, verified)
}

// SignMessage verifies a coin message from the device
// POST: /device/signmessage/{deviceid}
// Input: {"path": "<String>"m "message": <b64data>, "coin": <string>}
func (d *DeviceHandler) SignMessage(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	var signature devices.Signature
	err := json.NewDecoder(r.Body).Decode(&signature)
	log.Info("%v", signature)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	networkConstants := bip32.NetworkType(signature.Coin)
	if networkConstants.Name == "None" {
		sendError(w, errors.New("invalid coin type"), http.StatusBadRequest)
		return
	}

	if signature.Path == nil {
		sendError(w, errors.New("a path for the address must be provided"), http.StatusBadRequest)
		return
	}

	requestChan := make(chan PinRequired)
	pinRequest := func(needed bool) {
		pinChannel := d.pinChan[device.DeviceID]
		if pinChannel == nil {
			if needed {
				d.pinChan[device.DeviceID] = make(chan string)
				requestChan <- PinRequired{PinRequired: true}
			} else {
				requestChan <- PinRequired{PinRequired: false}
			}
		}
	}

	go device.SignMessage(signature.Path, signature.Message, networkConstants, pinRequest, d.responseToChanForDevice(device.DeviceID))
	response := <-requestChan
	sendResponse(w, response)
}

// SignTransaction verifies a coin message from the device
// POST: /device/signtransaction/{deviceid}
// Input: OutputTransaction structure
func (d *DeviceHandler) SignTransaction(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	var transaction bip32.OutputTransaction
	err := json.NewDecoder(r.Body).Decode(&transaction)
	log.Info("%v", transaction)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	networkConstants := bip32.NetworkType(transaction.Coin)
	if networkConstants.Name == "None" {
		sendError(w, errors.New("invalid coin type"), http.StatusBadRequest)
		return
	}

	requestChan := make(chan PinRequired)
	pinRequest := func(needed bool) {
		pinChannel := d.pinChan[device.DeviceID]
		if pinChannel == nil {
			if needed {
				d.pinChan[device.DeviceID] = make(chan string)
				requestChan <- PinRequired{PinRequired: true}
			} else {
				requestChan <- PinRequired{PinRequired: false}
			}
		}
	}

	//Sign the transaction
	go device.SignTx(&transaction, networkConstants, pinRequest, d.responseToChanForDevice(device.DeviceID))
	response := <-requestChan
	sendResponse(w, response)
}

// PublicKey returns the public key for a given coin type. This is the bip44 path
// that corresponds to that coin type
// GET: /device/{deviceid}/{coin}
func (d *DeviceHandler) PublicKey(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])
	coin := bip32.NetworkType(vars["coin"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// a coin type must be provided
	if coin == bip32.NoneNetwork {
		sendError(w, errors.New("coin type must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	// construct the bip44 path for the cointype
	path := []uint32{bip32.HardenedKey + 44, coin.Bip44Path, bip32.HardenedKey + 0}
	requestChan := make(chan PinRequired)
	pinRequest := func(needed bool) {
		pinChannel := d.pinChan[device.DeviceID]
		if pinChannel == nil {
			if needed {
				d.pinChan[device.DeviceID] = make(chan string)
				requestChan <- PinRequired{PinRequired: true}
			} else {
				requestChan <- PinRequired{PinRequired: false}
			}
		}
	}

	go device.GetPublicKey(path, pinRequest, d.responseToChanForDevice(device.DeviceID))
	response := <-requestChan
	sendResponse(w, response)
}

// MasterPublicKey returns the master public key for a given coin type.
// GET: /device/{deviceid}
func (d *DeviceHandler) MasterPublicKey(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	// if the device has a pending request then we will not process
	// anything. We should return an error.
	if d.responseChan[device.DeviceID] != nil {
		d.blockOnPendingResponse()
	}

	path := []uint32{}
	requestChan := make(chan PinRequired)
	pinRequest := func(needed bool) {
		pinChannel := d.pinChan[device.DeviceID]
		if pinChannel == nil {
			if needed {
				d.pinChan[device.DeviceID] = make(chan string)
				requestChan <- PinRequired{PinRequired: true}
			} else {
				requestChan <- PinRequired{PinRequired: false}
			}
		}
	}

	go device.GetPublicKey(path, pinRequest, d.responseToChanForDevice(device.DeviceID))
	response := <-requestChan
	sendResponse(w, response)
}

// Response returns a response from the device if there is a response waiting.
// Responses are returned after requests for pin (or from calls were a pin) would
// have been expected.
// GET: /device/response/{deviceid}
func (d *DeviceHandler) Response(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	channel := d.responseChan[device.DeviceID]
	if channel != nil {
		log.Info("sending device response")
		response := <-d.responseChan[device.DeviceID]
		d.responseChan[device.DeviceID] = nil

		switch response.(type) {
		case error:
			sendError(w, response.(error), http.StatusForbidden)
		default:
			sendResponse(w, response)
		}

		//if there are any waiting responses then remove it from the list and trigger it
		if len(d.waitingResponse) > 0 {
			log.Debug("there are pending responses: ", len(d.waitingResponse))
			d.waitingMutex.Lock()
			waiting := d.waitingResponse[0]
			d.waitingResponse = d.waitingResponse[1:]
			d.waitingMutex.Unlock()
			close(waiting)
		}

	} else {
		sendError(w, errors.New("no response is waiting for device"), http.StatusBadRequest)
	}
}

// Pin manages requests from the device for pin entry. The pin is
// sent in the path:
// POST: /device/pin/{deviceid}
// Expected: {"pin":"<pin>"}
func (d *DeviceHandler) Pin(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	device := d.getDevice(vars["deviceid"])

	// a valid device must be provided
	if device == nil {
		sendError(w, errors.New("a valid device must be provided"), http.StatusBadRequest)
		return
	}

	var pin pinRequest
	err := json.NewDecoder(r.Body).Decode(&pin)
	if err != nil {
		sendError(w, errors.New("invalid pin request"), http.StatusBadRequest)
	}

	pinChannel := d.pinChan[device.DeviceID]
	if pinChannel != nil {
		pinChannel <- pin.Pin
		sendResponse(w, "pin sent to device")
	} else {
		sendError(w, errors.New("device is not waiting for a pin"), http.StatusBadRequest)
	}
}

func (d *DeviceHandler) blockOnPendingResponse() {
	waitingChan := make(chan bool)
	d.waitingMutex.Lock()
	d.waitingResponse = append(d.waitingResponse, waitingChan)
	d.waitingMutex.Unlock()
	<-waitingChan
}

func (d *DeviceHandler) responseToChanForDevice(deviceID string) func(interface{}) {
	d.responseChan[deviceID] = make(chan interface{})
	return func(response interface{}) {
		log.Info("sending response to channel...")
		d.responseChan[deviceID] <- response
	}
}

func (d *DeviceHandler) pinFromChan(deviceID string) string {
	log.Info("waiting for pin...")

	pinChannel := d.pinChan[deviceID]
	if pinChannel != nil {
		pin := <-pinChannel
		d.pinChan[deviceID] = nil
		return pin
	}
	return ""
}

func (d *DeviceHandler) getDevice(deviceID string) *devices.TrezorDevice {
	for _, device := range d.devices {
		if device.DeviceID == deviceID {
			return device
		}
	}
	return nil
}

// SetupDeviceHandler returns a DeviceHandler to manage routes under
// the /device/* path
func SetupDeviceHandler() *DeviceHandler {
	log.Info("fetching devices")
	deviceHandler := &DeviceHandler{
		pinChan:         make(map[string]chan string),
		responseChan:    make(map[string]chan interface{}),
		waitingResponse: []chan bool{},
		waitingMutex:    &sync.Mutex{},
	}
	deviceHandler.devices = devices.GetDevices(deviceHandler.pinFromChan)
	return deviceHandler
}
