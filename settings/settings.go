package settings

// Settings is a container for the settings database
var Settings *KeyValueDB

// InitSettings initialises the settings database
func InitSettings() {
	Settings = CreateKeyValueDBWithFilename("settings.db")
}
