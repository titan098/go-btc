package settings

import (
	"encoding/json"
	"errors"

	"bitbucket.org/titan098/go-btc/logging"

	"github.com/boltdb/bolt"
)

var log = logging.SetupLogging("keyvaluedb")

// KeyValueDB is the containing structure for the persistent on disk cache
type KeyValueDB struct {
	db *bolt.DB
}

// PutJSON is a utility function which will marshal an interface to a byte array
// and store that in the cache. An error is returned if the value could not be
// marshalled. If the item was stored successfully then nil will be returned
func (c *KeyValueDB) PutJSON(identifier string, key string, input interface{}) error {
	data, err := json.Marshal(input)
	if err != nil {
		log.Warningf("input could not be marshalled to json: %s", key)
		return err
	}

	err = c.Put(identifier, key, data)
	if err != nil {
		log.Warningf("cache put error: %s", err.Error())
		return err
	}
	return nil
}

// GetJSON will retrieve a marshalled value from the cache. If the item could not be
// stored then an error is return, otherwise nil is returned
func (c *KeyValueDB) GetJSON(identifier string, key string, output interface{}) error {
	value, err := c.Get(identifier, key)
	if err != nil {
		return err
	}

	if value == nil {
		return errors.New("there is no cached value")
	}

	err = json.Unmarshal(value, output)
	if err != nil {
		log.Warningf("'%s' exists in the '%s' cache but could not be unmarshaled (%v)", key, identifier)
		log.Error(err.Error())
		return err
	}
	return nil
}

// PutString is a utility function which will marshal the string to a byte array
// and store that in the cache. An error is returned if the value could not be
// marshalled. If the item was stored successfully then nil will be returned
func (c *KeyValueDB) PutString(identifier string, key string, value string) error {
	data := []byte(value)

	err := c.Put(identifier, key, data)
	if err != nil {
		log.Warningf("cache put error: %s", err.Error())
		return err
	}
	return nil
}

// GetString will retrieve a marshalled value from the database. If the item could not be
// stored then an error is return, otherwise nil is returned
func (c *KeyValueDB) GetString(identifier string, key string) (string, error) {
	value, err := c.Get(identifier, key)
	if err != nil {
		return "", err
	}

	if value == nil {
		return "", errors.New("there is no cached value")
	}

	return string(value), nil
}

// Get will read a value from the cache. The []byte associated with the
// key is returned. An error is returned if the value cannot be read from
// the cache
func (c *KeyValueDB) Get(identifier string, key string) ([]byte, error) {
	bucketIdentifier := []byte(identifier)
	keyIdentifier := []byte(key)
	db := c.db

	var value []byte

	err := db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(bucketIdentifier)
		if bucket == nil {
			return errors.New("bucket not found")
		}

		value = bucket.Get(keyIdentifier)
		return nil
	})
	if err != nil {
		return nil, err
	}

	return value, nil
}

// GetWithDefault will read a value and return a defaulf if the value does not exists
// an error will be returned if there is an error reading the value
func (c *KeyValueDB) GetWithDefault(identifer string, key string, def []byte) ([]byte, error) {
	value, err := c.Get(identifer, key)
	if err != nil {
		return nil, err
	}

	if value == nil {
		return def, nil
	}
	return value, nil
}

// Put will place a value info the cache, keyed by key. If the value cannot
// be stored then an error will be returned.
func (c *KeyValueDB) Put(identifier string, key string, value []byte) error {
	bucketIdentifier := []byte(identifier)
	keyIdentifier := []byte(key)
	db := c.db

	err := db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists(bucketIdentifier)
		if err != nil {
			return err
		}

		err = bucket.Put(keyIdentifier, value)
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}

// Close will close the cache db.
func (c *KeyValueDB) Close() {
	c.db.Close()
}

// CreateKeyValueDB will create the cache and return a Cache struct
func CreateKeyValueDB() *KeyValueDB {
	return CreateKeyValueDBWithFilename("kv.db")
}

// CreateKeyValueDBWithFilename will create the cache and return a Cache struct
func CreateKeyValueDBWithFilename(file string) *KeyValueDB {
	log.Infof("opening '%s'", file)
	db, err := bolt.Open(file, 0600, nil)
	if err != nil {
		log.Error(err)
	}

	return &KeyValueDB{
		db: db,
	}
}
