package bip32

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"crypto/sha512"
	"encoding/binary"
	"errors"
	"math/big"

	"github.com/btcsuite/btcd/btcec"
	"github.com/cmars/basen"
	"golang.org/x/crypto/ripemd160"
)

// NetworkConstants holds the definitions for various network types that can be
// used as part of generating addresses and chains.
type NetworkConstants struct {
	Name      string `json:"name"`
	Public    uint32 `json:"public"`
	Private   uint32 `json:"private"`
	Version   byte   `json:"version"`
	Bip44Path uint32 `json:"bip44Path"`
	ShortCode string `json:"shortCode"`
}

// ExtendedPrivateKey holds the content of the extended private key. This will
// be a point and the chain code
type ExtendedPrivateKey struct {
	K *big.Int // key
	C *big.Int // chain-code
}

// ExtendedPublicKey holds the content of the extended public key. This will
// be the X,Y coordinate of the public key and the chain code.
type ExtendedPublicKey struct {
	Kx *big.Int // key X
	Ky *big.Int // Key y
	C  *big.Int // chain-code
}

// DerivedPrivateKey contains the information (or partial information) that
// is used to output the extended private key in xprv format.
type DerivedPrivateKey struct {
	Key         *ExtendedPrivateKey
	Parent      *ExtendedPrivateKey
	Depth       byte
	Child       uint32
	Version     uint32
	Fingerprint []byte
}

// DerivedPublicKey contains the information (or partial information) that
// is used to output the extended private key in xpub format.
type DerivedPublicKey struct {
	Key         *ExtendedPublicKey
	Parent      *ExtendedPublicKey
	Depth       byte
	Child       uint32
	Version     uint32
	Fingerprint []byte
}

var base58Encoding = basen.NewEncoding("123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz")
var secp256k1 = btcec.S256()

// BitcoinNetwork is the network constants for Bitcoin.
var BitcoinNetwork = NetworkConstants{Name: "Bitcoin", Public: 0x0488B21E, Private: 0x0488ADE4, Version: 0x00, Bip44Path: 0x80000000, ShortCode: "BTC"}

// TestnetNetwork is the network constants for Testnet.
var TestnetNetwork = NetworkConstants{Name: "Testnet", Public: 0x043587CF, Private: 0x04358394, Version: 0x6F, Bip44Path: 0x80000001, ShortCode: "TEST"}

// LitecoinNetwork is the network constants for Testnet.
var LitecoinNetwork = NetworkConstants{Name: "Litecoin", Public: 0x019D9CFE, Private: 0x019DA462, Version: 0x30, Bip44Path: 0x80000002, ShortCode: "LTC"}

// NoneNetwork is the network constants for None.
var NoneNetwork = NetworkConstants{Name: "None", Public: 0x0, Private: 0x0, Version: 0, Bip44Path: 0x0, ShortCode: "NONE"}

const (
	HardenedKey = uint32(0x80000000)
)

// ser32 serialises a 4-byte unsigned into into a 4 element byte array.
// The integer is stored as BigEndian
func ser32(p uint32) []byte {
	buf := make([]byte, 4)
	binary.BigEndian.PutUint32(buf, p)
	return buf
}

// ser256 serialises a 32 byte integer into a 32-byte, zero-padded array.
// The integer value is stored as BigEndian
func ser256(p *big.Int) []byte {
	buf := make([]byte, 32)
	b := p.Bytes()
	copy(buf[len(buf)-len(b):], b)

	return b
}

// serp converts a x, y coordinate into a compressed SEC point.
// the format is as follows <p>|<px>, where p is: 0x2 is <py> is even, or 0x3 if <py> is odd.
func serp(px *big.Int, py *big.Int) []byte {
	buf := bytes.Buffer{}

	even := py.Bit(0)
	b := byte(0x02)
	if even == 1 {
		b = byte(0x03)
	}

	buf.WriteByte(b)
	buf.Write(ser256(px))

	return buf.Bytes()
}

// decompressP converts a compressed point into an uncompressed point. An error is returned if the point
// cannot be decompressed.
func decompressP(data []byte) (*big.Int, *big.Int, error) {
	publicKey, err := btcec.ParsePubKey(data, secp256k1)
	if err != nil {
		return nil, nil, err
	}
	publicKeyRaw := publicKey.SerializeUncompressed()

	x := &big.Int{}
	y := &big.Int{}

	x.SetBytes(publicKeyRaw[1:33])
	y.SetBytes(publicKeyRaw[33:])

	return x, y, nil
}

// parse256 converts a 32-byte byte array into a integer value.
func parse256(p []byte) *big.Int {
	buf := big.Int{}
	buf.SetBytes(p)
	return &buf
}

// point converts a given private key p into the x, y coordinate of the
// corresponding public key.
func point(p *big.Int) (*big.Int, *big.Int) {
	x, y := secp256k1.ScalarBaseMult(p.Bytes())
	return x, y
}

// CKDpriv is the private Child Key Derivation function. Given a unsigned integers
// which references the index of the child, we will output an extended private key for that index.
func CKDpriv(k *ExtendedPrivateKey, i uint32) (*ExtendedPrivateKey, error) {
	h := hmac.New(sha512.New, k.C.Bytes())
	data := bytes.Buffer{}

	if i >= HardenedKey {
		data.WriteByte(0x00)
		data.Write(ser256(k.K))
		data.Write(ser32(i))
	} else {
		x, y := point(k.K)
		data.Write(serp(x, y))
		data.Write(ser32(i))
	}
	h.Write(data.Bytes())

	mac := h.Sum(nil)
	Il := parse256(mac[:32])
	Ir := parse256(mac[32:])

	var Ki big.Int
	Ki.Add(Il, k.K)
	Ki.Mod(&Ki, secp256k1.N)

	newExtendedKey := &ExtendedPrivateKey{
		K: &Ki,
		C: Ir,
	}

	return newExtendedKey, nil
}

// CKDpub is the public Child Key Derivation function. Given an unsigned integer
// which reference the index of the child, we will output an extended public key for that index.
// An error will be returned if the child index is a hardened index (> 2^31)
func CKDpub(k *ExtendedPublicKey, i uint32) (*ExtendedPublicKey, error) {
	if i >= HardenedKey {
		return nil, errors.New("Cannot derive from a Hardened Key")
	}

	h := hmac.New(sha512.New, k.C.Bytes())
	h.Write(serp(k.Kx, k.Ky))
	h.Write(ser32(i))

	mac := h.Sum(nil)
	Il := parse256(mac[:32])
	Ir := parse256(mac[32:])

	Ilx, Ily := point(Il)
	Kix, Kiy := secp256k1.Add(Ilx, Ily, k.Kx, k.Ky)

	newExtendedPublicKey := &ExtendedPublicKey{
		Kx: Kix,
		Ky: Kiy,
		C:  Ir,
	}

	return newExtendedPublicKey, nil
}

// N is the neuter function. Given an ExtendedPrivateKey this function will output the
// corresponding ExtendedPublicKey.
func N(k *ExtendedPrivateKey) *ExtendedPublicKey {
	if k == nil {
		return nil
	}

	Kx, Ky := point(k.K)
	newExtendedPublicKey := &ExtendedPublicKey{
		Kx: Kx,
		Ky: Ky,
		C:  k.C,
	}
	return newExtendedPublicKey
}

// derivePrivateKey generates a DerivedPrivateKey for a given path. This is performed through repeated
// application of doDerivePrivateKey.
func derivePrivateKey(m *ExtendedPrivateKey, tree []uint32) *DerivedPrivateKey {
	return doDerivePrivateKey(m, nil, 0, 0, tree)
}

// doDerivePrivateKey generates a DerivedPrivateKey for a given path while keeping track of the depth
// the tree and the current child.
func doDerivePrivateKey(m *ExtendedPrivateKey, parent *ExtendedPrivateKey, depth byte, child uint32, tree []uint32) *DerivedPrivateKey {
	if len(tree) == 0 {
		return &DerivedPrivateKey{
			Key:    m,
			Parent: parent,
			Depth:  depth,
			Child:  child,
		}
	}

	newM, _ := CKDpriv(m, tree[0])
	return doDerivePrivateKey(newM, m, depth+1, tree[0], tree[1:])
}

// DerivePublicKey generates a DerivedPublicKey for a given path. This is performed through repeated
// application of doDerivePublicKey.
func DerivePublicKey(m *ExtendedPublicKey, tree []uint32) *DerivedPublicKey {
	return doDerivePublicKey(m, nil, 0, 0, tree)
}

// doDerivePublicKey generates a DerivedPublicKey for a given path while keeping track of the depth
// the tree and the current child.
func doDerivePublicKey(m *ExtendedPublicKey, parent *ExtendedPublicKey, depth byte, child uint32, tree []uint32) *DerivedPublicKey {
	if len(tree) == 0 {
		return &DerivedPublicKey{
			Key:    m,
			Parent: parent,
			Depth:  depth,
			Child:  child,
		}
	}

	newM, _ := CKDpub(m, tree[0])
	return doDerivePublicKey(newM, m, depth+1, tree[0], tree[1:])
}

// generateMaster generates an ExtendedPrivateKey which corresponds to a given input seed.
// This is done through an HMAC-SHA512 to generate the initial private key and chain code.
func generateMaster(seed []byte) *ExtendedPrivateKey {
	key := []byte("Bitcoin seed")
	h := hmac.New(sha512.New, key)
	h.Write(seed)
	mac := h.Sum(nil)

	Il := parse256(mac[:32])
	Ir := parse256(mac[32:])

	return &ExtendedPrivateKey{
		K: Il,
		C: Ir,
	}
}

// generateFingerprint returns a 32-bit integer value as the fingerprint of the given
// ExtendedPublicKey. This corresponds to a hash160 of the public key.
func generateFingerprint(p *ExtendedPublicKey) []byte {
	address := hash160(serp(p.Kx, p.Ky))
	return address[:4]
}

// DecodePrivateKey takes an input byte array and returns the DerivedPrivateKey for that
// input data.
// !An error will be returned if the checksum does not match!
func DecodePrivateKey(data []byte) (*DerivedPrivateKey, error) {
	key := &DerivedPrivateKey{}

	key.Version = binary.BigEndian.Uint32(data[0:4])
	key.Depth = data[4]
	key.Fingerprint = data[5:9]
	key.Child = binary.BigEndian.Uint32(data[9:13])
	c := &big.Int{}
	c.SetBytes(data[13:45])
	k := &big.Int{}
	k.SetBytes(data[46:78])

	key.Key = &ExtendedPrivateKey{
		K: k,
		C: c,
	}

	return key, nil
}

// DecodePublicKey takes an input byte array and returns the DerivedPublicKey for that
// input data.
// !An error will be returned if the checksum does not match!
func DecodePublicKey(data []byte) (*DerivedPublicKey, error) {
	key := &DerivedPublicKey{}

	key.Version = binary.BigEndian.Uint32(data[0:4])
	key.Depth = data[4]
	key.Fingerprint = data[5:9]
	key.Child = binary.BigEndian.Uint32(data[9:13])
	c := &big.Int{}
	c.SetBytes(data[13:45])
	K := data[45:78]

	Kx, Ky, _ := decompressP(K)

	key.Key = &ExtendedPublicKey{
		Kx: Kx,
		Ky: Ky,
		C:  c,
	}

	return key, nil
}

// encodePrivateKey tasks an input DerivedPrivateKey and a set of network constants to generate
// xprv an xpub for that private key.
func encodePrivateKey(m *DerivedPrivateKey, network NetworkConstants) ([]byte, []byte) {
	version := network.Private
	publicDerivedKey := DerivedPublicKey{
		Key:    N(m.Key),
		Depth:  m.Depth,
		Child:  m.Child,
		Parent: N(m.Parent),
	}

	fingerprint := []byte{0, 0, 0, 0}
	if m.Parent != nil {
		fingerprint = generateFingerprint(N(m.Parent))
	}

	child := m.Child

	data := bytes.Buffer{}
	data.Write(ser32(version))
	data.WriteByte(m.Depth)
	data.Write(fingerprint)
	data.Write(ser32(child))
	data.Write(m.Key.C.Bytes())
	data.WriteByte(0x00)
	data.Write(m.Key.K.Bytes())

	return data.Bytes(), encodePublicKey(&publicDerivedKey, network)
}

// encodePublicKey task an input DerivedPublicKey and a set of NetworkConstants to generate
// the xpub for the input public key.
func encodePublicKey(m *DerivedPublicKey, network NetworkConstants) []byte {
	version := network.Public
	extendedPublicKey := m.Key

	fingerprint := []byte{0, 0, 0, 0}
	if m.Parent != nil {
		fingerprint = generateFingerprint(m.Parent)
	}

	child := m.Child

	data := bytes.Buffer{}
	data.Write(ser32(version))
	data.WriteByte(m.Depth)
	data.Write(fingerprint)
	data.Write(ser32(child))
	data.Write(extendedPublicKey.C.Bytes())
	data.Write(serp(extendedPublicKey.Kx, extendedPublicKey.Ky))

	return data.Bytes()
}

// hashSha256 produces a sha256 hash of the input byte array.
func hashSha256(data []byte) []byte {
	h := sha256.New()
	h.Write(data)
	return h.Sum(nil)
}

// hashDoubleSha256 produced a sha256(sha256(..)) of the input byte array.
func hashDoubleSha256(data []byte) []byte {
	return hashSha256(hashSha256(data))
}

// hashRipeMD160 produces a ripemd160(..) of the input byte array.
func hashRipeMD160(data []byte) []byte {
	h := ripemd160.New()
	h.Write(data)
	return h.Sum(nil)
}

// hash160 produces a ripemd160(sha256(..)) of the input byte array.
func hash160(data []byte) []byte {
	return hashRipeMD160(hashSha256(data))
}

// base58Encode encodes the input byte array as a bash-58 string.
func base58Encode(data []byte) string {
	return base58Encoding.EncodeToString(data)
}

// base58Decode decodes an input base-58 string into a byte array.
// An error is returned if data is not a valid base-58 string.
func base58Decode(data string) ([]byte, error) {
	return base58Encoding.DecodeString(data)
}

func base58EncodeWithVersion(data []byte, version byte) string {
	versionedData := make([]byte, len(data))
	copy(versionedData, data)

	// do the initial transformation of the leading zero's to the version bit
	versionBytes := 0
	for i, b := range versionedData {
		if b != 0 {
			break
		}
		versionedData[i] = version
		versionBytes++
	}

	b58 := base58Encode(versionedData)

	// if there were any the version bytes were 0 then the transformation
	// will have node nothing, just append with 1
	if version == 0 {
		for versionBytes > 0 {
			b58 = "1" + b58
			versionBytes--
		}
	}
	return b58
}

func base58DecodeWithVersion(data string, version byte) ([]byte, error) {
	decoded, err := base58Decode(data)
	if err != nil {
		return nil, err
	}

	// remove the version bytes to restore the zero padding
	// for i, b := range decoded {
	// 	if b == version {
	// 		decoded[i] = 0
	// 	}
	// }

	// if the original string was padded by ones we need to add bytes
	// for _, b := range data {
	// 	if b == '1' {
	// 		decoded = append([]byte{0x00}, decoded...)
	// 	} else {
	// 		break
	// 	}
	// }

	return decoded, nil

}

func checksum(data []byte) []byte {
	checksum := hashDoubleSha256(data)[:4]
	return checksum
}

// Base58Check creates the Base58 Check of the input data byte array.
// A Base58 check contains the data with the appended checksum with the
// leading zero padded with the version.
func Base58Check(data []byte, network NetworkConstants) string {
	buffer := bytes.Buffer{}
	buffer.Write(data)
	buffer.Write(checksum(data))

	s := base58EncodeWithVersion(buffer.Bytes(), network.Version)

	return s
}

// Base58DecodeCheck decodes the Base58 Check for the input string.
// An error is returned if the string cannot be decoded.
func Base58DecodeCheck(b58 string, network NetworkConstants) ([]byte, error) {
	buffer, err := base58DecodeWithVersion(b58, network.Version)
	if err != nil {
		return nil, err
	}

	data := buffer[:(len(buffer) - 4)]
	dataCheck := buffer[(len(buffer) - 4):]
	check := checksum(data)

	if binary.BigEndian.Uint32(dataCheck) != binary.BigEndian.Uint32(check) {
		return nil, (errors.New("Invalid checksum"))
	}
	return data, nil
}

// IsValidAddress returns true if this is a valid address for given network
func IsValidAddress(address string, network NetworkConstants) (bool, error) {
	_, err := base58DecodeWithVersion(address, network.Version)
	if err != nil {
		return false, err
	}
	return true, nil
}

// AddressFromPrivateKey calculates the address for the provided network
// constants. The address is returned as a string.
func AddressFromPrivateKey(priv *ExtendedPrivateKey, network NetworkConstants) string {
	return AddressFromPublicKey(N(priv), network)
}

// AddressFromPublicKey calculates the address for the provided network
// constants. The address is returned as a string.
func AddressFromPublicKey(pub *ExtendedPublicKey, network NetworkConstants) string {
	point := serp(pub.Kx, pub.Ky)
	return AddressFromPublicPoint(point, network)
}

// AddressFromPublicPoint calculates the address for the given network constants.
// The returned address will be a string
func AddressFromPublicPoint(point []byte, network NetworkConstants) string {
	h := hash160(point)
	addressBuffer := bytes.Buffer{}
	addressBuffer.WriteByte(byte(network.Version))
	addressBuffer.Write(h)
	address := Base58Check(addressBuffer.Bytes(), network)

	return address
}

// NetworkType returns a network type for a give name
func NetworkType(coinName string) NetworkConstants {
	switch coinName {
	case BitcoinNetwork.Name:
		return BitcoinNetwork
	case TestnetNetwork.Name:
		return TestnetNetwork
	case LitecoinNetwork.Name:
		return LitecoinNetwork
	default:
		return NoneNetwork
	}
}

// func main() {
// 	// key := "xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6LnF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi"
// 	// t, _ := Base58DecodeCheck(key, BitcoinNetwork)
// 	// y, _ := decodePrivateKey(t)
// 	// fmt.Println(y)
//
// 	key2 := "xpub6DDnpvn9upca5qDYLQAQWfANZmLFq2RakTQXSayd13eC4Yt8StCmuG296GB7Yt9w7tBhhuTpGxiALkSBXgiyJPdDgBXC4twuRbaCzqX85tq"
// 	q, _ := Base58DecodeCheck(key2, BitcoinNetwork)
// 	w, _ := decodePublicKey(q)
// 	n := derivePublicKey(w.Key, []uint32{0, 0})
//
// 	fmt.Println(AddressFromPublicKey(n.Key, BitcoinNetwork))
//
// 	//seed, _ := hex.DecodeString("000102030405060708090a0b0c0d0e0f")
// 	//m := N(generateMaster(seed))
// 	//i := []uint32{1}
// 	//m1 := derivePublicKey(m, i)
// 	//
// 	//pub := encodePublicKey(m1, BITCOIN_NETWORK)
// 	//fmt.Println(Base58Check(pub, BITCOIN_NETWORK))
// 	//fmt.Println(hex.EncodeToString(serp(m1.key.Kx, m1.key.Ky)))
// 	//fmt.Println(addressFromPublicKey(m1.key, BITCOIN_NETWORK))
// }
