package bip32

import (
	"errors"
	"math"
	"sort"
	"strings"
	"time"

	"bitbucket.org/titan098/go-btc/blockprovider"
	"bitbucket.org/titan098/go-btc/logging"
)

var log = logging.SetupLogging("account")

type addressInfomationMap struct {
	Addresses       map[int]string
	UnusedAddresses map[int]string
	UnusedIndex     []int
}

// Account is the primary structure which stores details for the account.
// This includes the provider as well as the transaction and balance details.
type Account struct {
	AccountRoot string           `json:"xpub"`
	Network     NetworkConstants `json:"network"`
	Path        []uint32         `json:"path"`
	Balance     uint64           `json:"balance"`

	provider     blockprovider.APIProvider
	internal     addressInfomationMap
	external     addressInfomationMap
	addressInfo  map[string]*blockprovider.AddressInformation
	addressTxs   map[string]*blockprovider.AddressTransactions
	transactions map[string]*blockprovider.Transaction
	decodedRoot  *DerivedPublicKey
}

type SendTransaction struct {
	FeePerByte uint64        `json:"feePerByte"`
	Unspent    UnspentOutput `json:"unspent"`
	Outputs    []struct {
		Address string `json:"address"`
		Amount  uint64 `json:"amount"`
	} `json:"outputs"`
}

type OutputTransaction struct {
	Vin  []blockprovider.TxIn  `json:"vin"`
	Vout []blockprovider.TxOut `json:"vout"`
	Fee  uint64                `json:"fee"`
	Coin string                `json:"coin"`
}

type ledgerEntry struct {
	Time       time.Time `json:"time"`
	TxID       string    `json:"txid"`
	Address    string    `json:"address"`
	Amount     uint64    `json:"amount"`
	Fee        uint64    `json:"fee"`
	In         bool      `json:"in"`
	Change     bool      `json:"change"`
	Unspent    bool      `json:"unspent"`
	AddressURL string    `json:"addressurl"`
	TxURL      string    `json:"txurl"`
}

//Ledger is a collection of ledgerEntrys. This is sortable by date.
type Ledger []ledgerEntry

func (l Ledger) Len() int {
	return len(l)
}

func (l Ledger) Less(i, j int) bool {
	return l[i].Time.After(l[j].Time)
}

func (l Ledger) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

type UnspentOutput []blockprovider.UnspentTx

func (u UnspentOutput) Len() int {
	return len(u)
}

func (u UnspentOutput) Less(i, j int) bool {
	return (u[i].Amount > u[j].Amount)
}

func (u UnspentOutput) Swap(i, j int) {
	u[i], u[j] = u[j], u[i]
}

func SatoshiToFloat(amount uint64) float64 {
	return float64(amount) / math.Pow10(8)
}

//Ledger returns the a collection of ledger entries for the account which
// outlines the funds which have flowed into this account
func (a *Account) Ledger() Ledger {
	log.Debug("constructing ledger for account")
	ledger := []ledgerEntry{}

	// group the transactions for in and out
	for address, addressTransactions := range a.addressTxs {
		for _, tx := range addressTransactions.TxIn {
			entry := ledgerEntry{
				TxID:       tx.TxID,
				Address:    address,
				Amount:     tx.Amount,
				Time:       tx.Date,
				Fee:        a.transactions[tx.TxID].Fee,
				In:         false,
				Unspent:    false,
				AddressURL: a.provider.GetAddressPublicURL(address),
				TxURL:      a.provider.GetTxPublicURL(tx.TxID),
			}
			ledger = append(ledger, entry)
		}
		for _, tx := range addressTransactions.TxOut {
			entry := ledgerEntry{
				TxID:       tx.TxID,
				Address:    address,
				Amount:     tx.Amount,
				Time:       tx.Date,
				Change:     a.IsInternal(address),
				In:         true,
				Unspent:    tx.Unspent,
				AddressURL: a.provider.GetAddressPublicURL(address),
				TxURL:      a.provider.GetTxPublicURL(tx.TxID),
			}
			ledger = append(ledger, entry)
		}
	}

	newLedger := make(Ledger, 0, len(ledger))
	for _, entry := range ledger {
		newLedger = append(newLedger, entry)
	}
	sort.Sort(newLedger)

	// display for debugging
	log.Debug("ledger for account:")
	for _, entry := range newLedger {
		direction := "-"
		chain := "E"
		spent := "S"
		if entry.In {
			direction = "+"
		}
		if !a.IsExternal(entry.Address) {
			chain = "I"
		}
		if entry.Unspent {
			spent = "U"
		}
		log.Debugf("%s\t%s\t%s%s\t%s\t%f\t%s%f", entry.Time, entry.TxID[0:8], chain, spent, entry.Address, SatoshiToFloat(entry.Fee), direction, SatoshiToFloat(entry.Amount))
	}

	return newLedger
}

//FetchAddress returns the internal and external addresses for the specified range.
func (a *Account) FetchAddress(start int, end int) (map[int]string, map[int]string) {
	external := map[int]string{}
	internal := map[int]string{}

	for i := start; i < end; i++ {
		externalPath := []uint32{0, uint32(i)}
		internalPath := []uint32{1, uint32(i)}

		externalAccount := DerivePublicKey(a.decodedRoot.Key, externalPath)
		internalAccount := DerivePublicKey(a.decodedRoot.Key, internalPath)

		externalAddress := AddressFromPublicKey(externalAccount.Key, a.Network)
		internalAddress := AddressFromPublicKey(internalAccount.Key, a.Network)

		external[i] = externalAddress
		internal[i] = internalAddress
	}

	return external, internal
}

//FetchAccountTransactions returns the transactions for the addreses in this account.
func (a *Account) FetchAccountTransactions() {
	log.Debugf("fetching account transactions for '%s'", a.AccountRoot)
	addresses := []string{}

	// get all of the keys for the account
	for k := range a.addressInfo {
		if a.addressInfo[k].NumberTransactions > 0 {
			addresses = append(addresses, k)
		}
	}

	// get the transaction information, this needs to be batched
	a.addressTxs = a.provider.FetchAddressTransactions(addresses)
}

// FetchAllTransactionDetails feches and stores all of the transaction details
// for the addresses in this account.
func (a *Account) FetchAllTransactionDetails() {
	log.Debugf("Fetching all transactions")
	transactions := []string{}
	for _, addressTransactions := range a.addressTxs {
		for _, tx := range addressTransactions.TxIn {
			transactions = append(transactions, tx.TxID)
		}
		for _, tx := range addressTransactions.TxOut {
			transactions = append(transactions, tx.TxID)
		}
	}

	a.transactions = a.provider.GetTransactions(transactions)
}

// getValueFromMap returns the values in a map of addreses.
func getValueFromMap(addressMap map[int]string) []string {
	log.Debug(addressMap)
	values := []string{}
	for _, value := range addressMap {
		values = append(values, value)
	}
	return values
}

// Refresh will refresh the account transactions, balance, unused addresses etc.
// This will call out to the specified provider to refresh the account information.
func (a *Account) Refresh() {
	log.Infof("Refreshing Account: '%s'", a.AccountRoot)
	errors := 0
	start := 0
	end := 5
	interval := 5

	a.Balance = 0.0
	a.external = addressInfomationMap{
		UnusedAddresses: map[int]string{},
		Addresses:       map[int]string{},
		UnusedIndex:     []int{},
	}
	a.internal = addressInfomationMap{
		UnusedAddresses: map[int]string{},
		Addresses:       map[int]string{},
		UnusedIndex:     []int{},
	}

	for (len(a.external.UnusedIndex) == 0 || len(a.internal.UnusedIndex) == 0) && (errors <= 10) {
		log.Debugf("Fetching from %v to %v", start, end)
		external, internal := a.FetchAddress(start, end)

		externalAddressInfo := a.provider.GetAddressInfo(getValueFromMap(external))
		internalAddressInfo := a.provider.GetAddressInfo(getValueFromMap(internal))

		for index, address := range external {
			if externalAddressInfo[address] != nil {
				a.external.Addresses[index] = address
				a.addressInfo[address] = externalAddressInfo[address]
				a.addressInfo[address].Index = index
				a.Balance += a.addressInfo[address].Balance
				log.Debugf("%s: %f (+%f)\n", address, SatoshiToFloat(a.Balance), SatoshiToFloat(a.addressInfo[address].Balance))

				if a.addressInfo[address].NumberTransactions == 0 {
					a.external.UnusedAddresses[index] = address
					a.external.UnusedIndex = append(a.external.UnusedIndex, index)
				}
			} else {
				errors++
				log.Warningf("There was no address info for: %s", address)
			}
		}

		for index, address := range internal {
			if internalAddressInfo[address] != nil {
				a.internal.Addresses[index] = address
				a.addressInfo[address] = internalAddressInfo[address]
				a.addressInfo[address].Index = index
				a.Balance += a.addressInfo[address].Balance
				log.Debugf("%s: %f (+%f)\n", address, SatoshiToFloat(a.Balance), SatoshiToFloat(a.addressInfo[address].Balance))

				if a.addressInfo[address].NumberTransactions == 0 {
					a.internal.UnusedAddresses[index] = address
					a.internal.UnusedIndex = append(a.internal.UnusedIndex, index)
				}
			} else {
				errors++
				log.Warningf("There was no address info for: %s", address)
			}
		}

		sort.Ints(a.external.UnusedIndex)
		sort.Ints(a.internal.UnusedIndex)
		start = end
		end += interval
	}
	log.Debug("final balance:", SatoshiToFloat(a.Balance))
}

func (a *Account) KnownAddress(address string) bool {
	return a.addressInfo[address] != nil
}

func (a *Account) IsExternal(address string) bool {
	for _, k := range a.external.Addresses {
		if k == address {
			return true
		}
	}
	return false
}

func (a *Account) IsInternal(address string) bool {
	for _, k := range a.internal.Addresses {
		if k == address {
			return true
		}
	}
	return false
}

func (a *Account) Unspent() UnspentOutput {
	log.Debug("finding unspent outputs for account")
	addresses := []string{}
	addresses = append(addresses, getValueFromMap(a.external.Addresses)...)
	addresses = append(addresses, getValueFromMap(a.internal.Addresses)...)
	unspent := UnspentOutput(a.provider.GetUnspentTransactions(addresses))

	sort.Sort(unspent)
	return unspent
}

// estimateFeePerByte will estimate how the amount of satoshi to includes
// as a transaction fee for the transaction
func estimateFeePerByte(inputs int, outputs int, feePerByte uint64) uint64 {
	if feePerByte == 0 {
		return 0
	}
	return uint64((10 + (inputs * 149) + (outputs * 35))) * feePerByte
}

// PathForAddress returns the bip32 path for the given address. If the address
// is found int the account from the valid address currently known.
// nil is returned if the address cannot be found
func (a *Account) PathForAddress(address string) []uint32 {
	addressPath := []uint32{}
	addressPath = append(addressPath, a.Path...)

	for index, externalAddress := range a.external.Addresses {
		if strings.Compare(externalAddress, address) == 0 {
			addressPath = append(addressPath, []uint32{0, uint32(index)}...)
			return addressPath
		}
	}

	for index, internalAddress := range a.internal.Addresses {
		if strings.Compare(internalAddress, address) == 0 {
			addressPath = append(addressPath, []uint32{1, uint32(index)}...)
			return addressPath
		}
	}

	return nil
}

func (a *Account) ReceiveAddress() []string {
	unusedAddresses := []string{}
	for _, address := range a.external.UnusedAddresses {
		unusedAddresses = append(unusedAddresses, address)
	}
	return unusedAddresses
}

func (a *Account) FeeEstimate(blocks []int) blockprovider.EstimateFee {
	return a.provider.GetEstimateFee(blocks)
}

func (a *Account) CreateTransaction(addresses *SendTransaction) (*OutputTransaction, error) {
	satoshiAmount := uint64(0)
	for _, sendAddresses := range addresses.Outputs {
		log.Infof("transaction %s: %v", sendAddresses.Address, sendAddresses.Amount)
		satoshiAmount += sendAddresses.Amount
	}

	totalAmount := uint64(0)
	changeAmount := uint64(0)
	feeSatoshi := addresses.FeePerByte
	feeAmount := estimateFeePerByte(1, len(addresses.Outputs)+1, feeSatoshi)

	unspentToUse := UnspentOutput{}

	var unspent UnspentOutput
	if len(addresses.Unspent) == 0 {
		unspent = a.Unspent()

		// find the transactions to use for sending. Select the minimum number of
		// transactions that will satisfy the output
		for _, unspentTx := range unspent {
			if totalAmount <= (satoshiAmount + feeAmount) {
				unspentTxAmount := unspentTx.Amount
				totalAmount += unspentTxAmount
				unspentToUse = append(unspentToUse, unspentTx)
				feeAmount = estimateFeePerByte(len(unspentToUse), len(addresses.Outputs)+1, feeSatoshi)
				changeAmount = totalAmount - (satoshiAmount + feeAmount)
			}
		}
	} else {
		// if the unspent outputs have been specified then we should use all of
		// them regardless of trying to minimize the transaction size
		unspent = addresses.Unspent

		for _, unspentTx := range unspent {
			totalAmount += unspentTx.Amount
			feeAmount = estimateFeePerByte(len(unspent), len(addresses.Outputs)+1, feeSatoshi)
			changeAmount = totalAmount - (satoshiAmount + feeAmount)
			unspentToUse = append(unspentToUse, unspentTx)
		}
	}

	//are we sending something
	if len(addresses.Outputs) == 0 {
		log.Error("there are no outputs")
		return nil, errors.New("there are no specified outputs")
	}

	// do we have enough to send
	if a.Balance <= 0.0 {
		log.Error("the account balance is 0")
		return nil, errors.New("the account balance is zero")
	}

	// do we have unspent outputs to consider
	if len(unspent) == 0 {
		log.Error("the account has not unspect outputs")
		return nil, errors.New("there are no unspent outputs")
	}

	// do we have enough to send
	if a.Balance < satoshiAmount {
		log.Errorf("balance of '%d' is not sufficient to send '%d'", a.Balance, satoshiAmount)
		return nil, errors.New("the balance cannot satisfy the amount to send")
	}

	// is the inputs sufficient to satisfy the outputs
	if totalAmount < satoshiAmount {
		log.Errorf("the unspent outputs cannot satisfy the amount to send")
		return nil, errors.New("unspent outputs not sufficient to satisfy the output")
	}

	// the tx doesn't consume all of the inputs
	if totalAmount != (satoshiAmount + changeAmount + feeAmount) {
		log.Errorf("the transaction does not consume all of the inputs")
		return nil, errors.New("the transaction does not consume all of the inputs")
	}

	// find an unused change address
	unusedChangeAddress := a.internal.UnusedAddresses[a.internal.UnusedIndex[0]]
	log.Debug("change address is:", unusedChangeAddress)
	log.Debug("number of unspent outputs:", len(unspentToUse))
	log.Debug("total value of inputs:", totalAmount)
	log.Debug("total amount:", satoshiAmount)
	log.Debug("change amount:", changeAmount)
	log.Debug("fee amount:", feeAmount)
	log.Debug("total+change+feeAmount == totalinput?", (totalAmount == (satoshiAmount + changeAmount + feeAmount)))

	outputTransaction := &OutputTransaction{}

	// append the unspent inputs to the transaction
	vins := []blockprovider.TxIn{}
	for _, unspentTx := range unspentToUse {
		txIn := blockprovider.TxIn{
			AddressN:  a.PathForAddress(unspentTx.Address),
			Address:   unspentTx.Address,
			Amount:    unspentTx.Amount,
			PrevHash:  unspentTx.Tx,
			PrevIndex: unspentTx.N,
			Script:    unspentTx.Script,
			RawTx:     a.provider.RawTransaction(unspentTx.Tx),
		}
		vins = append(vins, txIn)
	}

	// append each of the outputs to the transaction
	vouts := []blockprovider.TxOut{}
	for _, sendAddress := range addresses.Outputs {
		txOut := blockprovider.TxOut{
			Address: sendAddress.Address,
			Amount:  sendAddress.Amount,
			Change:  false,
		}
		vouts = append(vouts, txOut)
	}

	// if there is change then append the change transction
	if changeAmount > 0 {
		change := blockprovider.TxOut{
			Address:  unusedChangeAddress,
			AddressN: a.PathForAddress(unusedChangeAddress),
			Amount:   changeAmount,
			Change:   true,
		}
		vouts = append(vouts, change)
	}

	// record the transactions
	outputTransaction.Vin = vins
	outputTransaction.Vout = vouts
	outputTransaction.Fee = feeAmount
	outputTransaction.Coin = a.Network.Name

	// return!
	return outputTransaction, nil
}

// AccountFromXpub will construct an Account struct from a provided xpub. The
// serialised form of the xpub will always be "Bitcoin" encoded, however the
// subkeys should be determined by the root provided. An error is returned the
// key cannot be deseralised.
func AccountFromXpub(xpub string, network NetworkConstants, provider blockprovider.APIProvider) (*Account, error) {
	log.Infof("initilizing account '%s' for '%s'", xpub, network.Name)
	rawPublicKey, err := Base58DecodeCheck(xpub, BitcoinNetwork)
	if err != nil {
		return nil, err
	}

	decodedKey, err := DecodePublicKey(rawPublicKey)
	if err != nil {
		return nil, err
	}

	account := &Account{
		AccountRoot: xpub,
		Network:     network,
		Path:        []uint32{HardenedKey + 44, network.Bip44Path, HardenedKey + 0},
		decodedRoot: decodedKey,
		provider:    provider,

		Balance:     0.0,
		addressInfo: map[string]*blockprovider.AddressInformation{},
		addressTxs:  map[string]*blockprovider.AddressTransactions{},
		external: addressInfomationMap{
			UnusedAddresses: map[int]string{},
			Addresses:       map[int]string{},
			UnusedIndex:     []int{},
		},
		internal: addressInfomationMap{
			UnusedAddresses: map[int]string{},
			Addresses:       map[int]string{},
			UnusedIndex:     []int{},
		},
	}

	return account, nil
}

// func main() {
// 	// backend2 := logging.NewLogBackend(os.Stderr, "", 0)
// 	// backend2Formatter := logging.NewBackendFormatter(backend2, format)
// 	// logging.SetBackend(backend2Formatter)
// 	//
// 	log.Debug("Hello World")
//
// 	// account, err := AccountFromXpub("xpub6DDnpvn9upca5qDYLQAQWfANZmLFq2RakTQXSayd13eC4Yt8StCmuG296GB7Yt9w7tBhhuTpGxiALkSBXgiyJPdDgBXC4twuRbaCzqX85tq", BitcoinNetwork)
// 	account, err := AccountFromXpub("xpub6CHsYPUZx9BCG2uEZYmrUxqEJd6Coua3pr6vj4coTVSdMdtuhBaeMczLFUyRcT7aZEx1hfyY7KxUKTogshTs2p4hLoNWwpHLad2e9LgsQ45", TestnetNetwork, blockprovider.InsightProvider(blockprovider.InsightTestnet))
// 	if err != nil {
// 		log.Fatal(err)
// 	}
//
// 	account.Refresh()
// 	account.FetchAccountTransactions()
// 	account.FetchAllTransactionDetails()
// 	account.Ledger()
// 	account.Unspent()
// 	t, err := account.createTransaction("mmSKuHrmnkatxUCcx9VLDSt9cv3u4dqKYW", 1.0)
// 	if err != nil {
// 		log.Panic(err)
// 	}
// 	// d := account.buildDeviceTransaction(t)
// 	log.Debug(t)
// 	//
// 	// device := GetDevices()[0]
// 	// device.SignTx(t, account.Network)
// 	// fmt.Println(device)
//
// 	//provider := blockprovider.InsightProvider(blockprovider.InsightTestnet)
// 	// provider.GetAddressInfo([]string{"mmvP3mTe53qxHdPqXEvdu8WdC7GfQ2vmx5", "mmSKuHrmnkatxUCcx9VLDSt9cv3u4dqKYW"})
// 	// provider.GetTransactions([]string{"642b730d3bb90f8d4ba01e06ab0ee04738f2d88003c3f2a04e7492a68060eeff", "e6346a83461a5830b767c0f8b0546cd8525edf002fab94614ccea39749ef8540"})
// 	// provider.RawTransaction("642b730d3bb90f8d4ba01e06ab0ee04738f2d88003c3f2a04e7492a68060eeff")
// 	//provider.FetchAddressTransactions([]string{"n2GCcAgskMDz3DcMLUR8UcisGyGJrAcWG5"})
// }
