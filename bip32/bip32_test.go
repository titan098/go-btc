package bip32

import (
	"encoding/hex"
	"testing"

	"github.com/stretchr/testify/assert"
)

type Bip32Expected struct {
	key       string
	chaincode string
	xpriv     string
	xpub      string
	address   string
}

type Bip32TestSpec struct {
	seed     string
	path     []uint32
	network  NetworkConstants
	expected Bip32Expected
}

func TestXprvDecode(t *testing.T) {
	key := "xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6LnF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi"
	keyBytes, _ := Base58DecodeCheck(key, BitcoinNetwork)
	y, _ := DecodePrivateKey(keyBytes)

	assert.Equal(t, y.Depth, byte(0))
	assert.Equal(t, y.Child, uint32(0))
	assert.Equal(t, y.Fingerprint, []byte{0, 0, 0, 0})
	assert.Equal(t, y.Version, BitcoinNetwork.Private)
	assert.Equal(t, "873dff81c02f525623fd1fe5167eac3a55a049de3d314bb42ee227ffed37d508", hex.EncodeToString(y.Key.C.Bytes()))
	assert.Equal(t, "e8f32e723decf4051aefac8e2c93c9c5b214313817cdb01a1494b917c8436b35", hex.EncodeToString(y.Key.K.Bytes()))
}

func TestXpubDecode(t *testing.T) {
	key := "xpub661MyMwAqRbcFtXgS5sYJABqqG9YLmC4Q1Rdap9gSE8NqtwybGhePY2gZ29ESFjqJoCu1Rupje8YtGqsefD265TMg7usUDFdp6W1EGMcet8"
	keyBytes, _ := Base58DecodeCheck(key, BitcoinNetwork)
	y, _ := DecodePublicKey(keyBytes)

	assert.Equal(t, y.Depth, byte(0))
	assert.Equal(t, y.Child, uint32(0))
	assert.Equal(t, y.Fingerprint, []byte{0, 0, 0, 0})
	assert.Equal(t, y.Version, BitcoinNetwork.Public)
	assert.Equal(t, "873dff81c02f525623fd1fe5167eac3a55a049de3d314bb42ee227ffed37d508", hex.EncodeToString(y.Key.C.Bytes()))
	assert.Equal(t, "0339a36013301597daef41fbe593a02cc513d0b55527ec2df1050e2e8ff49c85c2", hex.EncodeToString(serp(y.Key.Kx, y.Key.Ky)))
}

func TestBase58EncodeWithVersion(t *testing.T) {
	test, _ := hex.DecodeString("00eb15231dfceb60925886b67d065299925915aeb172c06647")
	testB58 := base58EncodeWithVersion(test, BitcoinNetwork.Version)

	assert.Equal(t, "1NS17iag9jJgTHD1VXjvLCEnZuQ3rJDE9L", testB58)
}

func TestBase58DecodeWithVersionTestNet(t *testing.T) {
	test, _ := base58DecodeWithVersion("mqaRqU9e6RQ62BrnK8iWmhy5yzBvs4X36B", TestnetNetwork.Version)
	testB58 := hex.EncodeToString(test)

	assert.Equal(t, "6f6e597f0f05d3887e2f7a887b86f31cb7502c7b7b409919d4", testB58)
}

func TestIsValidAddress(t *testing.T) {
	valid, _ := Base58DecodeCheck("mqaRqU9e6RQ62BrnK8iWmhy5yzBvs4X36B", TestnetNetwork)
	testB58 := hex.EncodeToString(valid)
	assert.Equal(t, testB58, "6f6e597f0f05d3887e2f7a887b86f31cb7502c7b7b")
}

func TestBase58DecodeWithVersion(t *testing.T) {
	test, _ := base58DecodeWithVersion("1NS17iag9jJgTHD1VXjvLCEnZuQ3rJDE9L", BitcoinNetwork.Version)
	testB58 := hex.EncodeToString(test)

	assert.Equal(t, "eb15231dfceb60925886b67d065299925915aeb172c06647", testB58)
}

func TestBip32_pub_m(t *testing.T) {
	// Test: M
	doBip32TestPub(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "0339a36013301597daef41fbe593a02cc513d0b55527ec2df1050e2e8ff49c85c2",
			chaincode: "873dff81c02f525623fd1fe5167eac3a55a049de3d314bb42ee227ffed37d508",
			xpub:      "xpub661MyMwAqRbcFtXgS5sYJABqqG9YLmC4Q1Rdap9gSE8NqtwybGhePY2gZ29ESFjqJoCu1Rupje8YtGqsefD265TMg7usUDFdp6W1EGMcet8",
			address:   "15mKKb2eos1hWa6tisdPwwDC1a5J1y9nma",
		},
	})
}

func TestBip32_pub_m0(t *testing.T) {
	// Test: M/0
	doBip32TestPub(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{0},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "027c4b09ffb985c298afe7e5813266cbfcb7780b480ac294b0b43dc21f2be3d13c",
			chaincode: "d323f1be5af39a2d2f08f5e8f664633849653dbe329802e9847cfc85f8d7b52a",
			xpub:      "xpub68Gmy5EVb2BdFbj2LpWrk1M7obNuaPTpT5oh9QCCo5sRfqSHVYWex97WpDZzszdzHzxXDAzPLVSwybe4uPYkSk4G3gnrPqqkV9RyNzAcNJ1",
			address:   "1FHz8bpEE5qUZ9XhfjzAbCCwo5bT1HMNAc",
		},
	})
}

func TestBip32_pub_m1(t *testing.T) {
	// Test: M/1
	doBip32TestPub(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{1},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "037c2098fd2235660734667ff8821dbbe0e6592d43cfd86b5dde9ea7c839b93a50",
			chaincode: "8dd96414ff4d5b4750be3af7fecce207173f86d6b5f58f9366297180de8e109b",
			xpub:      "xpub68Gmy5EVb2BdHTYHpekwGdcbBWax19w9HwA2DaADYvuCSSgt4YAErxxSN1KWSnmyqkwRNbnTj3XiUBKmHeC8rTjLRPjSULcDKQQgfgJDppq",
			address:   "1J8QDN1u7iDMbJktbqXPSrAqruNjkmRFmT",
		},
	})
}

func TestBip32_m(t *testing.T) {
	// Test: m
	doBip32TestPriv(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "e8f32e723decf4051aefac8e2c93c9c5b214313817cdb01a1494b917c8436b35",
			chaincode: "873dff81c02f525623fd1fe5167eac3a55a049de3d314bb42ee227ffed37d508",
			xpriv:     "xprv9s21ZrQH143K3QTDL4LXw2F7HEK3wJUD2nW2nRk4stbPy6cq3jPPqjiChkVvvNKmPGJxWUtg6LnF5kejMRNNU3TGtRBeJgk33yuGBxrMPHi",
			xpub:      "xpub661MyMwAqRbcFtXgS5sYJABqqG9YLmC4Q1Rdap9gSE8NqtwybGhePY2gZ29ESFjqJoCu1Rupje8YtGqsefD265TMg7usUDFdp6W1EGMcet8",
			address:   "15mKKb2eos1hWa6tisdPwwDC1a5J1y9nma",
		},
	})
}

func TestBip32_m0p(t *testing.T) {
	// Test: m/0'
	doBip32TestPriv(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{HardenedKey},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "edb2e14f9ee77d26dd93b4ecede8d16ed408ce149b6cd80b0715a2d911a0afea",
			chaincode: "47fdacbd0f1097043b78c63c20c34ef4ed9a111d980047ad16282c7ae6236141",
			xpriv:     "xprv9uHRZZhk6KAJC1avXpDAp4MDc3sQKNxDiPvvkX8Br5ngLNv1TxvUxt4cV1rGL5hj6KCesnDYUhd7oWgT11eZG7XnxHrnYeSvkzY7d2bhkJ7",
			xpub:      "xpub68Gmy5EdvgibQVfPdqkBBCHxA5htiqg55crXYuXoQRKfDBFA1WEjWgP6LHhwBZeNK1VTsfTFUHCdrfp1bgwQ9xv5ski8PX9rL2dZXvgGDnw",
			address:   "19Q2WoS5hSS6T8GjhK8KZLMgmWaq4neXrh",
		},
	})
}

func TestBip32_m0p1(t *testing.T) {
	// Test: m/0'/1
	doBip32TestPriv(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{HardenedKey, 1},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "3c6cb8d0f6a264c91ea8b5030fadaa8e538b020f0a387421a12de9319dc93368",
			chaincode: "2a7857631386ba23dacac34180dd1983734e444fdbf774041578e9b6adb37c19",
			xpriv:     "xprv9wTYmMFdV23N2TdNG573QoEsfRrWKQgWeibmLntzniatZvR9BmLnvSxqu53Kw1UmYPxLgboyZQaXwTCg8MSY3H2EU4pWcQDnRnrVA1xe8fs",
			xpub:      "xpub6ASuArnXKPbfEwhqN6e3mwBcDTgzisQN1wXN9BJcM47sSikHjJf3UFHKkNAWbWMiGj7Wf5uMash7SyYq527Hqck2AxYysAA7xmALppuCkwQ",
			address:   "1JQheacLPdM5ySCkrZkV66G2ApAXe1mqLj",
		},
	})
}

func TestBip32_m0p12p(t *testing.T) {
	// Test: m/0'/1/2'
	doBip32TestPriv(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{HardenedKey, 1, HardenedKey + 2},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "cbce0d719ecf7431d88e6a89fa1483e02e35092af60c042b1df2ff59fa424dca",
			chaincode: "04466b9cc8e161e966409ca52986c584f07e9dc81f735db683c3ff6ec7b1503f",
			xpriv:     "xprv9z4pot5VBttmtdRTWfWQmoH1taj2axGVzFqSb8C9xaxKymcFzXBDptWmT7FwuEzG3ryjH4ktypQSAewRiNMjANTtpgP4mLTj34bhnZX7UiM",
			xpub:      "xpub6D4BDPcP2GT577Vvch3R8wDkScZWzQzMMUm3PWbmWvVJrZwQY4VUNgqFJPMM3No2dFDFGTsxxpG5uJh7n7epu4trkrX7x7DogT5Uv6fcLW5",
			address:   "1NjxqbA9aZWnh17q1UW3rB4EPu79wDXj7x",
		},
	})
}

func TestBip32_m0p12p2(t *testing.T) {
	// Test: m/0'/1/2'/2
	doBip32TestPriv(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{HardenedKey, 1, HardenedKey + 2, 2},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "0f479245fb19a38a1954c5c7c0ebab2f9bdfd96a17563ef28a6a4b1a2a764ef4",
			chaincode: "cfb71883f01676f587d023cc53a35bc7f88f724b1f8c2892ac1275ac822a3edd",
			xpriv:     "xprvA2JDeKCSNNZky6uBCviVfJSKyQ1mDYahRjijr5idH2WwLsEd4Hsb2Tyh8RfQMuPh7f7RtyzTtdrbdqqsunu5Mm3wDvUAKRHSC34sJ7in334",
			xpub:      "xpub6FHa3pjLCk84BayeJxFW2SP4XRrFd1JYnxeLeU8EqN3vDfZmbqBqaGJAyiLjTAwm6ZLRQUMv1ZACTj37sR62cfN7fe5JnJ7dh8zL4fiyLHV",
			address:   "1LjmJcdPnDHhNTUgrWyhLGnRDKxQjoxAgt",
		},
	})
}

func TestBip32_m0p12p21000000000(t *testing.T) {
	// Test: m/0'/1/2'/2/1000000000
	doBip32TestPriv(t, &Bip32TestSpec{
		seed:    "000102030405060708090a0b0c0d0e0f",
		path:    []uint32{HardenedKey, 1, HardenedKey + 2, 2, 1000000000},
		network: BitcoinNetwork,
		expected: Bip32Expected{
			key:       "471b76e389e528d6de6d816857e012c5455051cad6660850e58372a6c3e6e7c8",
			chaincode: "c783e67b921d2beb8f6b389cc646d7263b4145701dadd2161548a8b078e65e9e",
			xpriv:     "xprvA41z7zogVVwxVSgdKUHDy1SKmdb533PjDz7J6N6mV6uS3ze1ai8FHa8kmHScGpWmj4WggLyQjgPie1rFSruoUihUZREPSL39UNdE3BBDu76",
			xpub:      "xpub6H1LXWLaKsWFhvm6RVpEL9P4KfRZSW7abD2ttkWP3SSQvnyA8FSVqNTEcYFgJS2UaFcxupHiYkro49S8yGasTvXEYBVPamhGW6cFJodrTHy",
			address:   "1LZiqrop2HGR4qrH1ULZPyBpU6AUP49Uam",
		},
	})
}

func doBip32TestPriv(t *testing.T, spec *Bip32TestSpec) {
	seed, _ := hex.DecodeString(spec.seed)
	m := generateMaster(seed)
	mk := derivePrivateKey(m, spec.path)
	priv, pub := encodePrivateKey(mk, spec.network)

	computedKey := hex.EncodeToString(mk.Key.K.Bytes())
	computeChainCode := hex.EncodeToString(mk.Key.C.Bytes())
	computedxpriv := Base58Check(priv, spec.network)
	computedxpub := Base58Check(pub, spec.network)
	computedaddress := AddressFromPrivateKey(mk.Key, spec.network)

	assert.Equal(t, spec.expected.key, computedKey)
	assert.Equal(t, spec.expected.chaincode, computeChainCode)
	assert.Equal(t, spec.expected.xpriv, computedxpriv)
	assert.Equal(t, spec.expected.xpub, computedxpub)
	assert.Equal(t, spec.expected.address, computedaddress)
}

func doBip32TestPub(t *testing.T, spec *Bip32TestSpec) {
	seed, _ := hex.DecodeString(spec.seed)
	m := generateMaster(seed)
	mk := DerivePublicKey(N(m), spec.path)
	pub := encodePublicKey(mk, spec.network)

	computedKey := hex.EncodeToString(serp(mk.Key.Kx, mk.Key.Ky))
	computeChainCode := hex.EncodeToString(mk.Key.C.Bytes())
	computedxpub := Base58Check(pub, spec.network)
	computedaddress := AddressFromPublicKey(mk.Key, spec.network)

	assert.Equal(t, spec.expected.key, computedKey)
	assert.Equal(t, spec.expected.chaincode, computeChainCode)
	assert.Equal(t, spec.expected.xpub, computedxpub)
	assert.Equal(t, spec.expected.address, computedaddress)
}
