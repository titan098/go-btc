package blockprovider

import (
	"net/http"
	"strconv"
	"strings"
	"time"
)

// --- INPUT STURCTURES ---
type blockrRawTx struct {
	Status string `json:"status"`
	Data   struct {
		Tx struct {
			Hex      string `json:"hex"`
			Txid     string `json:"txid"`
			Hash     string `json:"hash"`
			Size     int    `json:"size"`
			Vsize    int    `json:"vsize"`
			Version  int    `json:"version"`
			Locktime int    `json:"locktime"`
			Vin      []struct {
				Txid      string `json:"txid"`
				Vout      int    `json:"vout"`
				ScriptSig struct {
					Asm string `json:"asm"`
					Hex string `json:"hex"`
				} `json:"scriptSig"`
				Sequence int64 `json:"sequence"`
			} `json:"vin"`
			Vout []struct {
				Value        float64 `json:"value"`
				N            int     `json:"n"`
				ScriptPubKey struct {
					Asm       string   `json:"asm"`
					Hex       string   `json:"hex"`
					ReqSigs   int      `json:"reqSigs"`
					Type      string   `json:"type"`
					Addresses []string `json:"addresses"`
				} `json:"scriptPubKey"`
			} `json:"vout"`
			Blockhash     string `json:"blockhash"`
			Confirmations int    `json:"confirmations"`
			Time          int    `json:"time"`
			Blocktime     int    `json:"blocktime"`
		} `json:"tx"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type blockrTx struct {
	Status string `json:"status"`
	Data   []struct {
		Tx            string    `json:"tx"`
		Block         uint32    `json:"block"`
		Confirmations uint32    `json:"confirmations"`
		TimeUtc       time.Time `json:"time_utc"`
		IsCoinbased   int       `json:"is_coinbased"`
		Trade         struct {
			Vins []struct {
				Address       string  `json:"address"`
				IsNonstandard bool    `json:"is_nonstandard"`
				Amount        float64 `json:"amount"`
				N             int     `json:"n"`
				Type          int     `json:"type"`
				VoutTx        string  `json:"vout_tx"`
			} `json:"vins"`
			Vouts []struct {
				Address       string  `json:"address"`
				IsNonstandard bool    `json:"is_nonstandard"`
				Amount        float64 `json:"amount"`
				N             int     `json:"n"`
				Type          int     `json:"type"`
				IsSpent       int     `json:"is_spent"`
			} `json:"vouts"`
		} `json:"trade"`
		Vins []struct {
			Address       string `json:"address"`
			IsNonstandard bool   `json:"is_nonstandard"`
			Amount        string `json:"amount"`
			N             int    `json:"n"`
			Type          int    `json:"type"`
			VoutTx        string `json:"vout_tx"`
		} `json:"vins"`
		Vouts []struct {
			Address       string `json:"address"`
			IsNonstandard bool   `json:"is_nonstandard"`
			Amount        string `json:"amount"`
			N             int    `json:"n"`
			Type          int    `json:"type"`
			IsSpent       int    `json:"is_spent"`
			Extras        struct {
				Asm     string `json:"asm"`
				Script  string `json:"script"`
				ReqSigs int    `json:"reqSigs"`
				Type    string `json:"type"`
			} `json:"extras"`
		} `json:"vouts"`
		Fee           string      `json:"fee"`
		DaysDestroyed string      `json:"days_destroyed"`
		IsUnconfirmed bool        `json:"is_unconfirmed"`
		Extras        interface{} `json:"extras"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type blockrUnspentTx struct {
	Status string `json:"status"`
	Data   struct {
		Address string `json:"address"`
		Unspent []struct {
			Tx            string `json:"tx"`
			Amount        string `json:"amount"`
			N             int    `json:"n"`
			Confirmations int    `json:"confirmations"`
			Script        string `json:"script"`
		} `json:"unspent"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type blockrAddressInfo struct {
	Status string `json:"status"`
	Data   []struct {
		Address         string  `json:"address"`
		IsUnknown       bool    `json:"is_unknown"`
		Balance         float64 `json:"balance"`
		BalanceMultisig int     `json:"balance_multisig"`
		Totalreceived   float64 `json:"totalreceived"`
		NbTxs           int     `json:"nb_txs"`
		FirstTx         struct {
			TimeUtc       time.Time `json:"time_utc"`
			Tx            string    `json:"tx"`
			BlockNb       string    `json:"block_nb"`
			Value         float64   `json:"value"`
			Confirmations int       `json:"confirmations"`
		} `json:"first_tx"`
		LastTx struct {
			TimeUtc       time.Time `json:"time_utc"`
			Tx            string    `json:"tx"`
			BlockNb       string    `json:"block_nb"`
			Value         float64   `json:"value"`
			Confirmations int       `json:"confirmations"`
		} `json:"last_tx"`
		IsValid bool `json:"is_valid"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type blockrAddressTxs struct {
	Status string `json:"status"`
	Data   []struct {
		Address        string `json:"address"`
		LimitTxs       int    `json:"limit_txs"`
		NbTxs          int    `json:"nb_txs"`
		NbTxsDisplayed int    `json:"nb_txs_displayed"`
		Txs            []struct {
			Tx             string    `json:"tx"`
			TimeUtc        time.Time `json:"time_utc"`
			Confirmations  int       `json:"confirmations"`
			Amount         float64   `json:"amount"`
			AmountMultisig int       `json:"amount_multisig"`
		} `json:"txs"`
	} `json:"data"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// Blockr is the main containing structure for a Blockr provider
type Blockr struct {
	client  *http.Client
	baseURL string
}

const (
	//BlockrBitcoin is the main API endpoint for the BTC api
	BlockrBitcoin = "https://btc.blockr.io/api/v1/"

	//BlockrTestnet is the testnet API endpoint for the TBTC api
	BlockrTestnet = "https://tbtc.blockr.io/api/v1/"

	//BlockrLitecoin is the litecoin  API endpoint for the LTC api
	BlockrLitecoin = "https://ltc.blockr.io/api/v1/"
)

// BlockrProvider Return an initialisd blockr endpoint. The apiEndpoint is the
// baseurl for the api.
func BlockrProvider(apiEndpoint string) Blockr {
	return Blockr{
		baseURL: apiEndpoint,
		client:  &http.Client{Timeout: time.Second * 30},
	}
}

// GetAddressInfo returns the information for an address.
func (b Blockr) GetAddressInfo(address []string) map[string]*AddressInformation {
	start := 0
	increment := 15
	end := increment
	addressInfoArray := map[string]*AddressInformation{}

	for start < len(address) {
		if end > len(address) {
			end = len(address)
		}
		batchAddressArray := b.doGetAddressInfo(address[start:end])
		for address, addressInfo := range batchAddressArray {
			addressInfoArray[address] = addressInfo
		}
		start = end
		end += increment
	}

	return addressInfoArray
}

// doGetAddressInfo retrieves the basic address information and returnes
// it as a map of AddressInformation keyed by address.
func (b Blockr) doGetAddressInfo(address []string) map[string]*AddressInformation {
	addressString := strings.Join(address, ",")
	addressString += ","
	url := b.baseURL + "address/info/" + addressString
	log.Debug("fetching address information:", url)

	addressInfo := &blockrAddressInfo{}
	err := GetJSON(b.client, url, addressInfo)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	// transform to egress structure
	addressInfoArray := map[string]*AddressInformation{}

	for _, info := range addressInfo.Data {
		if info.Address != "" {
			addressInformation := &AddressInformation{
				Address:            info.Address,
				Balance:            FloatToSatoshi(info.Balance),
				NumberTransactions: info.NbTxs,
			}
			addressInfoArray[info.Address] = addressInformation
		}
	}

	return addressInfoArray
}

// FetchAddressTransactions retrieve the transaction summary for the
// specified address list. The requests will be batched
func (b Blockr) FetchAddressTransactions(address []string) map[string]*AddressTransactions {
	start := 0
	increment := 15
	end := increment
	addressTxInfo := map[string]*AddressTransactions{}

	for start < len(address) {
		if end > len(address) {
			end = len(address)
		}
		batchTxArray := b.doFetchAddressTransactions(address[start:end])
		for address, addressTransactions := range batchTxArray {
			addressTxInfo[address] = addressTransactions
		}
		start = end
		end += increment
	}

	return addressTxInfo
}

// doFetchAddressTransactions retrieves and serialises a list of address transactions.
// The transactions are returned as a map keyed by address
func (b Blockr) doFetchAddressTransactions(address []string) map[string]*AddressTransactions {
	addressTxMap := map[string]*AddressTransactions{}
	accountTransactions := b.getAddressTransactions(address)

	for _, addressTx := range accountTransactions.Data {
		if addressTx.Address != "" && addressTx.NbTxs > 0 {
			inTx := []TransactionInformation{}
			outTx := []TransactionInformation{}

			for _, tx := range addressTx.Txs {
				transaction := TransactionInformation{
					TxID:   tx.Tx,
					Date:   tx.TimeUtc,
					Amount: FloatToSatoshi(tx.Amount),
				}

				if tx.Amount >= 0 {
					inTx = append(inTx, transaction)
				} else {
					outTx = append(outTx, transaction)
				}
			}

			addressTransactions := &AddressTransactions{
				TxIn:  inTx,
				TxOut: outTx,
			}
			addressTxMap[addressTx.Address] = addressTransactions
		}
	}

	return addressTxMap
}

// GetAddressTransactions returns the transactions for the address
func (b Blockr) getAddressTransactions(address []string) blockrAddressTxs {
	addressString := strings.Join(address, ",")
	addressString += ","
	url := b.baseURL + "address/txs/" + addressString
	log.Debug("fetching address transactions:", url)

	addressTxs := &blockrAddressTxs{}
	err := GetJSON(b.client, url, addressTxs)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	return *addressTxs
}

// GetTransactions fetches the details of a list of transactions.
// The transactions are returned as a map of transactions keyed by transaction ID
func (b Blockr) GetTransactions(transactions []string) map[string]*Transaction {
	start := 0
	increment := 15
	end := increment
	transactionMap := map[string]*Transaction{}

	for start < len(transactions) {
		if end > len(transactions) {
			end = len(transactions)
		}
		batchTransactionsMap := b.doGetTransactions(transactions[start:end])
		for tx, transaction := range batchTransactionsMap {
			transactionMap[tx] = transaction
		}
		start = end
		end += increment
	}

	return transactionMap
}

// doGetTransactions fetches a the transactions and returneds the map
// of transactions keyed by address.
func (b Blockr) doGetTransactions(transactions []string) map[string]*Transaction {
	txMap := map[string]*Transaction{}
	tx := b.getTx(transactions)

	for _, transaction := range tx.Data {
		feeAmount, _ := strconv.ParseFloat(transaction.Fee, 64)
		newTransaction := &Transaction{
			ID:            transaction.Tx,
			Confirmations: transaction.Confirmations,
			Date:          transaction.TimeUtc,
			Fee:           FloatToSatoshi(feeAmount),
		}
		newTxIn := []TxIn{}
		newTxOut := []TxOut{}

		for _, vin := range transaction.Vins {
			amount, _ := strconv.ParseFloat(vin.Amount, 64)
			newVin := TxIn{
				Address:   vin.Address,
				Amount:    FloatToSatoshi(amount),
				PrevIndex: vin.N,
				PrevHash:  vin.VoutTx,
			}
			newTxIn = append(newTxIn, newVin)
		}

		for _, vout := range transaction.Vouts {
			amount, _ := strconv.ParseFloat(vout.Amount, 64)
			newVout := TxOut{
				TxID:        transaction.Tx,
				Address:     vout.Address,
				Amount:      FloatToSatoshi(amount),
				Index:       vout.N,
				Script:      vout.Extras.Script,
				Unspent:     (vout.IsSpent == 0x30),
				BlockHeight: transaction.Block,
				Coinbase:    (transaction.IsCoinbased == 0x31),
			}
			newTxOut = append(newTxOut, newVout)
		}

		newTransaction.Vins = newTxIn
		newTransaction.Vout = newTxOut

		txMap[transaction.Tx] = newTransaction
	}

	return txMap
}

// RawTransaction returns the raw transaction for a given transaction identifer.
func (b Blockr) RawTransaction(txID string) string {
	rawTx := b.getRawTx(txID)
	return rawTx.Data.Tx.Hex
}

// GetTx returns the transaction information for an input transaction id.
func (b Blockr) getTx(transactions []string) blockrTx {
	transactionsString := strings.Join(transactions, ",")
	transactionsString += ","
	url := b.baseURL + "tx/info/" + transactionsString
	log.Debug("fetching transactions:", url)

	txInfo := &blockrTx{}
	err := GetJSON(b.client, url, txInfo)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	return *txInfo
}

// GetUnspentTx returns the unspent transactions for a given address.
func (b Blockr) GetUnspentTx(address string) map[string][]*UnspentTx {
	unspentTx := map[string][]*UnspentTx{}
	url := b.baseURL + "address/unspent/" + address
	log.Debug("fetching unspect transactions:", url)

	txUnspent := &blockrUnspentTx{}
	err := GetJSON(b.client, url, txUnspent)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	UnspentTxArray := []*UnspentTx{}
	for _, utxo := range txUnspent.Data.Unspent {
		amount, _ := strconv.ParseFloat(utxo.Amount, 64)
		a := &UnspentTx{
			Tx:            utxo.Tx,
			Amount:        FloatToSatoshi(amount),
			N:             utxo.N,
			Confirmations: utxo.Confirmations,
			Script:        utxo.Script,
		}
		UnspentTxArray = append(UnspentTxArray, a)
	}
	unspentTx[address] = UnspentTxArray

	return unspentTx
}

// GetRawTx returns the raw transaction for a given transaction identifer.
func (b Blockr) getRawTx(tx string) blockrRawTx {
	url := b.baseURL + "tx/raw/" + tx

	txInfo := &blockrRawTx{}
	err := GetJSON(b.client, url, txInfo)
	log.Debug("fetching raw transaction tx:", url)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	return *txInfo
}

// IsSupported returns true if the coin type is supported by this provider
func (b Blockr) IsSupported(coinType string) bool {
	switch coinType {
	case "btc":
		return true
	case "tbtc":
		return true
	case "ltc":
		return true
	default:
		return false
	}
}
