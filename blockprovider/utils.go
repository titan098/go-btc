package blockprovider

import (
	"encoding/json"
	"io/ioutil"
	"math"
	"net/http"
	"time"

	"bitbucket.org/titan098/go-btc/logging"
)

var log = logging.SetupLogging("insight")

// --- API Provider Interface ---

// APIProvider interface describes the interaction with an api provider
type APIProvider interface {
	GetAddressInfo(address []string) map[string]*AddressInformation
	FetchAddressTransactions(address []string) map[string]*AddressTransactions
	GetTransactions(transactions []string) map[string]*Transaction
	RawTransaction(txID string) string
	GetUnspentTransactions(addresses []string) []UnspentTx

	IsSupported(coinType string) bool

	GetAddressPublicURL(address string) string
	GetTxPublicURL(tx string) string
	GetEstimateFee(nblocks []int) EstimateFee
}

// --- OUTPUT STRUCTURES ---

// AddressInformation contains the basic information relating to an
// address.
type AddressInformation struct {
	Address            string
	Balance            uint64
	NumberTransactions int
	Index              int
}

// TransactionInformation returns the basic information for a transaction.
type TransactionInformation struct {
	TxID    string
	Date    time.Time
	Amount  uint64
	Unspent bool
}

// AddressTransactions contains the transactions for the input and
// oputput transactions for an address.
type AddressTransactions struct {
	TxIn  []TransactionInformation
	TxOut []TransactionInformation
}

// TxIn represents an input transaction.
type TxIn struct {
	AddressN  []uint32 `json:"path,omitempty"`
	Index     uint32   `json:"n"`
	Address   string   `json:"address"`
	Amount    uint64   `json:"amount"`
	PrevIndex int      `json:"previndex"`
	PrevHash  string   `json:"prevhash"`
	Script    string   `json:"script"`
	RawTx     string   `json:"rawtx"`
}

// TxOut represents an output transaction
type TxOut struct {
	TxID        string   `json:"txid,omitempty"`
	Address     string   `json:"address"`
	AddressN    []uint32 `json:"path,omitempty"`
	Amount      uint64   `json:"amount"`
	Index       int      `json:"n"`
	Script      string   `json:"script,omitempty"`
	Unspent     bool     `json:"unspent,omitempty"`
	Change      bool     `json:"change"`
	BlockHeight uint32   `json:"blockHeight,omitempty"`
	Coinbase    bool     `json:"coinbase,omitempty"`
	RawTx       string   `json:"rawtx,omitempty"`
}

// Transaction represents the main body of a transaction
type Transaction struct {
	ID            string
	Date          time.Time
	Confirmations uint32
	Vins          []TxIn
	Vout          []TxOut
	Fee           uint64
}

// UnspentTx represents a set of unspent transactions for an address
type UnspentTx struct {
	Tx            string `json:"txid"`
	Amount        uint64 `json:"amount"`
	Address       string `json:"address"`
	N             int    `json:"n"`
	Confirmations int    `json:"confirmations"`
	Script        string `json:"script"`
}

// RawTransaction represents a raw transaction
type RawTransaction struct {
	RawTx string `json:"rawTx"`
}

type ParallelUrlResult struct {
	key    string
	url    string
	status int
	result []byte
	err    error
}

type ParallelUrlRequest struct {
	key string
	url string
}

type EstimateFee map[int]uint64

// FloatToSatoshi converts a float value to a satoshi value
func FloatToSatoshi(value float64) uint64 {
	satoshiValue := uint64(math.Trunc(value * math.Pow(10, 8)))
	return satoshiValue
}

// GetJSONParallel will execute the requests to a set of urls in parallel
// and collate the results
func GetJSONParallel(client *http.Client, urls []ParallelUrlRequest) map[string]*ParallelUrlResult {
	// if there is nothing to do... dont do anything
	log.Debug("starting parallel get...")
	if len(urls) == 0 {
		return map[string]*ParallelUrlResult{}
	}

	start := 0
	increment := 5
	end := start + increment
	if end > len(urls) {
		end = len(urls)
	}

	urlBatch := urls[start:end]
	results := map[string]*ParallelUrlResult{}
	for len(urlBatch) > 0 {
		resultsChan := make(chan *ParallelUrlResult, len(urlBatch))
		done := make(chan bool)
		for _, url := range urlBatch {
			go GetHTTPResult(client, url, resultsChan, done, len(urlBatch))
		}
		<-done

		for elem := range resultsChan {
			results[elem.url] = elem
		}

		// fetch the next batch
		start = end
		end += increment
		if end > len(urls) {
			// if there aren't enough then adjust end to the last element
			end = len(urls)
		}
		urlBatch = urls[start:end]
	}
	return results
}

// GetHTTPResult reads the JSON from the target URL and populates the
// result in the corresponding target. An error is returned if there
// was an error fetching or parsing the JSON.
func GetHTTPResult(client *http.Client, url ParallelUrlRequest, resultChannel chan *ParallelUrlResult, done chan bool, expectedResults int) {
	log.Debugf("get http result... (%s)", url.url)
	r, err := client.Get(url.url)
	if err != nil {
		resultChannel <- &ParallelUrlResult{
			err: err,
		}
		closeParallelChannels(resultChannel, done, expectedResults)
		return
	}
	defer r.Body.Close()

	log.Debugf("fetching: %s (%s)", url, r.Status)
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		resultChannel <- &ParallelUrlResult{
			status: r.StatusCode,
			err:    err,
		}
		closeParallelChannels(resultChannel, done, expectedResults)
		return
	}

	resultChannel <- &ParallelUrlResult{
		key:    url.key,
		url:    url.url,
		status: r.StatusCode,
		result: body,
	}
	closeParallelChannels(resultChannel, done, expectedResults)
	return
}

func closeParallelChannels(resultChannel chan *ParallelUrlResult, done chan bool, expectedResults int) {
	if len(resultChannel) >= expectedResults {
		log.Debug("closing parallel fetch channels")
		close(resultChannel)
		close(done)
	}
}

// GetJSON reads the JSON from the target URL and populates the
// result in the corresponding target. An error is returned if there
// was an error fetching or parsing the JSON.
func GetJSON(client *http.Client, url string, target interface{}) error {
	r, err := client.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}
