package blockprovider

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/titan098/go-btc/settings"
)

// --- INPUT STURCTURES ---
type insightRawTx struct {
	Rawtx string `json:"rawtx"`
}

type insightTx struct {
	Txid     string `json:"txid"`
	Version  int    `json:"version"`
	Locktime int    `json:"locktime"`
	Vin      []struct {
		Txid      string `json:"txid"`
		Vout      int    `json:"vout"`
		Sequence  int64  `json:"sequence"`
		N         int    `json:"n"`
		ScriptSig struct {
			Hex string `json:"hex"`
			Asm string `json:"asm"`
		} `json:"scriptSig"`
		Addr            string      `json:"addr"`
		ValueSat        uint64      `json:"valueSat"`
		Value           float64     `json:"value"`
		DoubleSpentTxID interface{} `json:"doubleSpentTxID"`
	} `json:"vin"`
	Vout []struct {
		Value        string `json:"value"`
		N            int    `json:"n"`
		ScriptPubKey struct {
			Hex       string   `json:"hex"`
			Asm       string   `json:"asm"`
			Addresses []string `json:"addresses"`
			Type      string   `json:"type"`
		} `json:"scriptPubKey"`
		SpentTxID   interface{} `json:"spentTxId"`
		SpentIndex  interface{} `json:"spentIndex"`
		SpentHeight interface{} `json:"spentHeight"`
	} `json:"vout"`
	Blockhash     string  `json:"blockhash"`
	Blockheight   uint32  `json:"blockheight"`
	Confirmations uint32  `json:"confirmations"`
	Time          int64   `json:"time"`
	Blocktime     int     `json:"blocktime"`
	ValueOut      float64 `json:"valueOut"`
	IsCoinBase    bool    `json:"isCoinBase"`
	Size          int     `json:"size"`
	ValueIn       float64 `json:"valueIn"`
	Fees          float64 `json:"fees"`
}

type insightUnspentTx []struct {
	Address       string  `json:"address"`
	Txid          string  `json:"txid"`
	Vout          int     `json:"vout"`
	ScriptPubKey  string  `json:"scriptPubKey"`
	Amount        float64 `json:"amount"`
	Satoshis      uint64  `json:"satoshis"`
	Height        int     `json:"height"`
	Confirmations int     `json:"confirmations"`
}

type insightAddressInfo struct {
	AddrStr                 string   `json:"addrStr"`
	Balance                 float64  `json:"balance"`
	BalanceSat              uint64   `json:"balanceSat"`
	TotalReceived           float64  `json:"totalReceived"`
	TotalReceivedSat        uint64   `json:"totalReceivedSat"`
	TotalSent               float64  `json:"totalSent"`
	TotalSentSat            uint64   `json:"totalSentSat"`
	UnconfirmedBalance      int      `json:"unconfirmedBalance"`
	UnconfirmedBalanceSat   int      `json:"unconfirmedBalanceSat"`
	UnconfirmedTxApperances int      `json:"unconfirmedTxApperances"`
	TxApperances            int      `json:"txApperances"`
	Transactions            []string `json:"transactions"`
}

type insightAddressTx struct {
	TotalItems int         `json:"totalItems"`
	From       int         `json:"from"`
	To         int         `json:"to"`
	Items      []insightTx `json:"items"`
}

type insightEstimateFee map[string]float64

// Insight is the main containing structure for a Blockr provider
type Insight struct {
	client *http.Client
	coin   string
	cache  *settings.KeyValueDB
}

var defaultCoinURI map[string]string
var defaultPublicCoinURI map[string]string

const (
	//InsightBitcoin is the main API endpoint for the BTC api
	//InsightBitcoin = "https://insight.bitpay.com/api/"
	InsightBitcoin       = "http://microserver.local:3003/insight-api/"
	InsightBitcoinPublic = "http://microserver.local:3003/insight/"

	//InsightTestnet is the testnet API endpoint for the TBTC api
	//InsightTestnet = "https://test-insight.bitpay.com/api/"
	InsightTestnet       = "http://microserver.local:3001/insight-api/"
	InsightTestnetPublic = "http://microserver.local:3001/insight/"

	//InsightLitecoin is the litecoin API endpoint for the TBTC api
	//InsightTestnet = "https://test-insight.bitpay.com/api/"
	InsightLitecoin       = "http://microserver.local:3002/insight-lite-api/"
	InsightLitecoinPublic = "http://microserver.local:3002/insight/"
)

// InsightProvider Return an initialisd blockr endpoint. The apiEndpoint is the
// baseurl for the api.
func InsightProvider(coin string, cache *settings.KeyValueDB) Insight {
	if defaultCoinURI == nil {
		defaultCoinURI = make(map[string]string)
		defaultCoinURI["Bitcoin"] = InsightBitcoin
		defaultCoinURI["Testnet"] = InsightTestnet
		defaultCoinURI["Litecoin"] = InsightLitecoin
	}
	if defaultPublicCoinURI == nil {
		defaultPublicCoinURI = make(map[string]string)
		defaultPublicCoinURI["Bitcoin"] = InsightBitcoinPublic
		defaultPublicCoinURI["Testnet"] = InsightTestnetPublic
		defaultPublicCoinURI["Litecoin"] = InsightLitecoinPublic
	}

	return Insight{
		coin:   coin,
		client: &http.Client{Timeout: time.Second * 10},
		cache:  cache,
	}
}

// GetEndpointForCoin will return the endpoint being used with this provider
func (b Insight) GetEndpointForCoin() string {
	output, err := settings.Settings.GetString(b.coin, "endpoint")
	if err != nil {
		return defaultCoinURI[b.coin]
	}
	return output
}

// GetPublicEndpointForCoin will return the display endpoint being used
// with this provider
func (b Insight) GetPublicEndpointForCoin() string {
	output, err := settings.Settings.GetString(b.coin, "endpointpublic")
	if err != nil {
		return defaultPublicCoinURI[b.coin]
	}
	return output
}

// GetAddressPublicURL return the display URL for an address
func (b Insight) GetAddressPublicURL(address string) string {
	return b.GetPublicEndpointForCoin() + "address/" + address
}

// GetTxPublicURL returns the display url for a transaction
func (b Insight) GetTxPublicURL(tx string) string {
	return b.GetPublicEndpointForCoin() + "tx/" + tx
}

// GetAddressInfo returns the information for an address.
func (b Insight) GetAddressInfo(address []string) map[string]*AddressInformation {
	log.Debug("Get address information")
	urls := []ParallelUrlRequest{}
	for _, addr := range address {
		url := ParallelUrlRequest{
			key: addr,
			url: b.GetEndpointForCoin() + "addr/" + addr,
		}
		urls = append(urls, url)
	}
	a := GetJSONParallel(b.client, urls)

	addressInfoArray := map[string]*AddressInformation{}
	for _, result := range a {
		if result.err == nil {
			addressInfo := &insightAddressInfo{}
			err := json.Unmarshal(result.result, addressInfo)
			if err != nil {
				log.Error("json could not be decoded: ", err.Error())
			}

			addressInformation := &AddressInformation{
				Address:            addressInfo.AddrStr,
				Balance:            addressInfo.BalanceSat,
				NumberTransactions: addressInfo.TxApperances,
			}
			addressInfoArray[addressInfo.AddrStr] = addressInformation
		} else {
			log.Errorf("error: %s", result.err.Error())
		}
	}

	return addressInfoArray
}

// FetchAddressTransactions retrieve the transaction summary for the
// specified address list. The requests will be batched
func (b Insight) FetchAddressTransactions(addresses []string) map[string]*AddressTransactions {
	addressTxMap := map[string]*AddressTransactions{}
	accountTransactions := b.getAddressTransactions(addresses)

	for _, address := range addresses {
		addressTx := accountTransactions[address]
		inTx := []TransactionInformation{}
		outTx := []TransactionInformation{}
		for _, addressTx := range addressTx {
			// is this an output from this account, a transaction
			// with this address as a vin
			var value uint64
			date := time.Unix(addressTx.Time, 0)
			for _, tx := range addressTx.Vin {
				if tx.Addr == address {
					log.Debugf("vin %s, -%d", address, tx.ValueSat)
					value = tx.ValueSat

					transaction := TransactionInformation{
						TxID:   addressTx.Txid,
						Date:   date,
						Amount: value,
					}
					inTx = append(inTx, transaction)
				}
			}

			// is this an input transaction? a transaction with
			// this address and an output
			for _, tx := range addressTx.Vout {
				if len(tx.ScriptPubKey.Addresses) > 0 && (tx.ScriptPubKey.Addresses[0] == address) {
					log.Debugf("vout %s, +%s, %v", tx.ScriptPubKey.Addresses[0], tx.Value, tx.SpentTxID == nil)
					v, _ := strconv.ParseFloat(tx.Value, 64)
					value = FloatToSatoshi(v)

					transaction := TransactionInformation{
						TxID:    addressTx.Txid,
						Date:    date,
						Amount:  value,
						Unspent: tx.SpentTxID == nil,
					}
					outTx = append(outTx, transaction)
				}
			}
		}

		addressTransactions := &AddressTransactions{
			TxIn:  inTx,
			TxOut: outTx,
		}
		addressTxMap[address] = addressTransactions
	}

	return addressTxMap
}

// getAddressTransactions returns the transactions for the address
func (b Insight) getAddressTransactions(addresses []string) map[string][]insightTx {
	transactions := make(map[string][]insightTx)
	addressToProcess := []string{}
	from := 0
	to := 20
	increment := 20

	for _, address := range addresses {
		addressToProcess = append(addressToProcess, address)
	}

	for len(addressToProcess) > 0 {
		newAddressesToProcess := []string{}

		// construct the list of urls
		urls := []ParallelUrlRequest{}
		for _, address := range addressToProcess {
			url := ParallelUrlRequest{
				key: address,
				url: b.GetEndpointForCoin() + "addrs/" + address + "/txs?from=" + strconv.Itoa(from) + "&to=" + strconv.Itoa(to),
			}
			urls = append(urls, url)
		}

		// fetch the results
		results := GetJSONParallel(b.client, urls)
		for _, result := range results {
			addressTx := &insightAddressTx{}
			err := json.Unmarshal(result.result, addressTx)
			if err != nil {
				log.Error("json could not be decoded: ", err.Error())
			}

			addressForTx := result.key
			addressTransaction := []insightTx{}
			for _, tx := range addressTx.Items {
				addressTransaction = append(addressTransaction, tx)
			}

			// append the address transactions for this address
			if transactions[addressForTx] == nil {
				transactions[addressForTx] = []insightTx{}
			}
			transactions[addressForTx] = append(transactions[addressForTx], addressTransaction...)

			//do we need to process this address in the next round?
			if addressTx.To != addressTx.TotalItems {
				newAddressesToProcess = append(newAddressesToProcess, addressForTx)
			}
		}

		// adjust increment range
		addressToProcess = newAddressesToProcess
		from = to
		to += increment
	}

	// return the final list of transactions per address
	return transactions
}

// GetTransactions fetches the details of a list of transactions.
// The transactions are returned as a map of transactions keyed by transaction ID
func (b Insight) GetTransactions(transactions []string) map[string]*Transaction {
	log.Infof("there are %d transactions", len(transactions))
	txMap := map[string]*Transaction{}
	txToFetch := []string{}

	// iterate over the set of transactions and pull the cached items
	// if the transaction is not in the cache then we must fetch it
	for _, transaction := range transactions {
		newTransaction := &Transaction{}
		err := b.cache.GetJSON("tx", transaction, newTransaction)
		if err != nil {
			txToFetch = append(txToFetch, transaction)
		} else {
			log.Debugf("tx: %v (cached)", newTransaction.ID)
			txMap[transaction] = newTransaction
		}
	}

	// transactions which were not in the cache need to be fetched
	fetchedTx := b.getTx(txToFetch)
	for _, tx := range fetchedTx {
		newTransaction := &Transaction{
			ID:            tx.Txid,
			Confirmations: tx.Confirmations,
			Date:          time.Unix(tx.Time, 0),
			Fee:           FloatToSatoshi(tx.Fees),
		}
		newTxIn := []TxIn{}
		newTxOut := []TxOut{}

		for _, vin := range tx.Vin {
			newVin := TxIn{
				Address:   vin.Addr,
				Amount:    vin.ValueSat,
				PrevIndex: vin.N,
				PrevHash:  vin.Txid,
			}
			newTxIn = append(newTxIn, newVin)
		}
		for _, vout := range tx.Vout {
			amount, _ := strconv.ParseFloat(vout.Value, 64)
			if amount > 0 {
				newVout := TxOut{
					TxID:        tx.Txid,
					Address:     vout.ScriptPubKey.Addresses[0],
					Amount:      FloatToSatoshi(amount),
					Index:       vout.N,
					Script:      vout.ScriptPubKey.Hex,
					Unspent:     (vout.SpentTxID == nil),
					BlockHeight: tx.Blockheight,
					Coinbase:    tx.IsCoinBase,
					RawTx:       b.RawTransaction(tx.Txid),
				}

				newTxOut = append(newTxOut, newVout)
			}
		}

		newTransaction.Vins = newTxIn
		newTransaction.Vout = newTxOut
		b.cache.PutJSON("tx", tx.Txid, newTransaction)
		txMap[tx.Txid] = newTransaction
	}
	log.Debugf("all transactions fetched: %v transactions", len(txMap))

	return txMap
}

// GetTx returns the transaction information for an input transaction id.
func (b Insight) getTx(transactions []string) []*insightTx {
	urls := []ParallelUrlRequest{}
	for _, tx := range transactions {
		url := ParallelUrlRequest{
			key: tx,
			url: b.GetEndpointForCoin() + "tx/" + tx,
		}
		urls = append(urls, url)
	}
	results := GetJSONParallel(b.client, urls)

	resultsArray := []*insightTx{}
	for _, result := range results {
		tx := &insightTx{}
		err := json.Unmarshal(result.result, tx)
		if err != nil {
			log.Error("json could not be decoded: ", err.Error())
		}

		resultsArray = append(resultsArray, tx)
	}

	return resultsArray
}

// RawTransaction returns the raw transaction for a given transaction identifer.
func (b Insight) RawTransaction(txID string) string {
	tx := &RawTransaction{}
	err := b.cache.GetJSON("rawtx", txID, tx)
	if err != nil {
		//there was no cached value for this item
		rawTx := b.getRawTx(txID)
		tx.RawTx = rawTx.Rawtx
		b.cache.PutJSON("rawtx", txID, tx)
	}
	return tx.RawTx
}

func (b Insight) getRawTx(tx string) *insightRawTx {
	url := b.GetEndpointForCoin() + "rawtx/" + tx
	log.Debug("fetching raw transaction tx:", url)
	txInfo := &insightRawTx{}
	err := GetJSON(b.client, url, txInfo)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	return txInfo
}

// GetUnspentTransactions returns a list of unspect transations for the
// set of passed addresses
func (b Insight) GetUnspentTransactions(addresses []string) []UnspentTx {
	unspentTx := []UnspentTx{}
	for _, address := range addresses {
		utxo := b.GetUnspentTx(address)
		unspentTx = append(unspentTx, utxo...)
	}
	return unspentTx
}

// GetUnspentTx returns the unspent transactions for a given address.
func (b Insight) GetUnspentTx(address string) []UnspentTx {
	unspentTx := []UnspentTx{}
	url := b.GetEndpointForCoin() + "addr/" + address + "/utxo"

	txUnspent := insightUnspentTx{}
	err := GetJSON(b.client, url, &txUnspent)
	log.Debug("fetching unspent transaction txs:", url)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	UnspentTxArray := []UnspentTx{}
	for _, utxo := range txUnspent {
		a := UnspentTx{
			Address:       address,
			Tx:            utxo.Txid,
			Amount:        FloatToSatoshi(utxo.Amount),
			N:             utxo.Vout,
			Confirmations: utxo.Confirmations,
			Script:        utxo.ScriptPubKey,
		}
		UnspentTxArray = append(UnspentTxArray, a)
	}
	unspentTx = append(unspentTx, UnspentTxArray...)

	return unspentTx
}

// GetEstimateFee get the estimate fee for a block range
func (b Insight) GetEstimateFee(nblocks []int) EstimateFee {
	var ids []string
	for _, i := range nblocks {
		ids = append(ids, strconv.Itoa(i))
	}

	url := b.GetEndpointForCoin() + "utils/estimatefee?nbBlocks=" + strings.Join(ids, ",")

	insightEstimate := insightEstimateFee{}
	err := GetJSON(b.client, url, &insightEstimate)
	log.Debug("fetching fee estimate:", url)
	if err != nil {
		log.Error("json could not be decoded: ", err.Error())
	}

	estimateFee := EstimateFee{}
	for blocks, fee := range insightEstimate {
		intBlocks, _ := strconv.Atoi(blocks)
		if fee > -1 {
			satoshiFee := FloatToSatoshi(fee / 1024)
			estimateFee[intBlocks] = satoshiFee
		}
	}

	return estimateFee
}

// IsSupported returns true if the coin type is supported by this provider
func (b Insight) IsSupported(coinType string) bool {
	switch coinType {
	case "btc":
		return true
	case "tbtc":
		return true
	case "ltc":
		return true
	default:
		return false
	}
}
