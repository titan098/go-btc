package main

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"sync"

	"bitbucket.org/titan098/go-btc/bip32"
	"bitbucket.org/titan098/go-btc/blockprovider"
	"bitbucket.org/titan098/go-btc/settings"

	"github.com/gorilla/mux"
)

type AccountHandler struct {
	accounts map[string][]*bip32.Account

	refreshLock map[*bip32.Account]*sync.Mutex
	cache       *settings.KeyValueDB
	providers   map[bip32.NetworkConstants]blockprovider.APIProvider
}

type AddressList struct {
	Internal *map[int]string `json:"internal"`
	External *map[int]string `json:"external"`
}

type BlockList []int

// providerForCoin will return a provider for the coin type. This will
// create a new provider if required and return the value newly created provider
// to the caller.
func (a *AccountHandler) providerForCoin(coin bip32.NetworkConstants) blockprovider.APIProvider {
	provider := a.providers[coin]
	if provider != nil {
		return provider
	}

	// we need to construct a provider for the coin type
	provider = blockprovider.InsightProvider(coin.Name, a.cache)

	// return the provider to the user
	a.providers[coin] = provider
	return provider
}

func (a *AccountHandler) getRefreshLock(account *bip32.Account) *sync.Mutex {
	refreshLock := a.refreshLock[account]
	if refreshLock == nil {
		refreshLock = &sync.Mutex{}
		a.refreshLock[account] = refreshLock
	}
	return refreshLock
}

func (a *AccountHandler) refreshAccountTransactions(account *bip32.Account, complete bool) {
	refreshLock := a.getRefreshLock(account)

	refreshLock.Lock()
	account.Refresh()
	if complete {
		account.FetchAccountTransactions()
		account.FetchAllTransactionDetails()
	}
	refreshLock.Unlock()
}

func (a *AccountHandler) checkVars(r *http.Request) (string, bip32.NetworkConstants, error) {
	vars := mux.Vars(r)
	xpub := vars["xpub"]
	coin := bip32.NetworkType(vars["coin"])

	// a valid xpub must be provided
	if xpub == "" {
		return "", bip32.NoneNetwork, errors.New("a valid xpub must be provided")
	}

	// a coin type must be provided
	if coin == bip32.NoneNetwork {
		return "", bip32.NoneNetwork, errors.New("coin type must be provided")
	}

	return xpub, coin, nil
}

func (a *AccountHandler) accountForCoin(coin bip32.NetworkConstants, xpub string) (*bip32.Account, error) {
	accountArray := a.accounts[coin.Name]
	if accountArray == nil {
		return nil, errors.New("no accounts of the coin type were found")
	}

	for _, account := range accountArray {
		if account.AccountRoot == xpub {
			return account, nil
		}
	}

	return nil, errors.New("no xpub for the coin type was found")
}

func (a *AccountHandler) Init(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err == nil {
		log.Debug("Account for coin already exists, returning...")
		sendResponse(w, account)
		return
	}

	account, err = bip32.AccountFromXpub(xpub, coin, a.providerForCoin(coin))
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	if a.accounts[coin.Name] == nil {
		a.accounts[coin.Name] = []*bip32.Account{}
	}
	a.accounts[coin.Name] = append(a.accounts[coin.Name], account)

	sendResponse(w, account)
}

func (a *AccountHandler) List(w http.ResponseWriter, r *http.Request) {
	sendResponse(w, a.accounts)
}

func (a *AccountHandler) Refresh(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	a.refreshAccountTransactions(account, true)
	sendResponse(w, account)
}

func (a *AccountHandler) RefreshBalance(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	a.refreshAccountTransactions(account, false)
	sendResponse(w, account)
}

func (a *AccountHandler) Get(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	sendResponse(w, account)
}

func (a *AccountHandler) Ledger(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	ledger := account.Ledger()
	sendResponse(w, ledger)
}

func (a *AccountHandler) Unspent(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	unspent := account.Unspent()
	sendResponse(w, unspent)
}

func (a *AccountHandler) ReceiveAddress(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	receiveAddresses := account.ReceiveAddress()
	sendResponse(w, receiveAddresses)
}

func (a *AccountHandler) EstimateFee(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	var blockList BlockList
	err = json.NewDecoder(r.Body).Decode(&blockList)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	estimateFee := account.FeeEstimate(blockList)
	sendResponse(w, estimateFee)
}

func (a *AccountHandler) Address(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	start, err := strconv.Atoi(vars["start"])
	if err != nil {
		sendError(w, errors.New("start is not a valid integer"), http.StatusBadRequest)
		return
	}

	end, err := strconv.Atoi(vars["end"])
	if err != nil {
		sendError(w, errors.New("start is not a valid integer"), http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	external, internal := account.FetchAddress(start, end)
	sendResponse(w, &AddressList{
		Internal: &internal,
		External: &external,
	})
}

func (a *AccountHandler) Path(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	address := vars["address"]

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	path := account.PathForAddress(address)
	if path == nil {
		sendError(w, errors.New("address is unknown in the account"), http.StatusBadRequest)
		return
	}

	sendResponse(w, path)
}

func (a *AccountHandler) CheckAddress(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	coin := vars["coin"]
	address := vars["address"]
	networkConstants := bip32.NetworkType(coin)

	if networkConstants == bip32.NoneNetwork {
		sendError(w, errors.New("a coin type must be provided"), http.StatusBadRequest)
		return
	}
	log.Debug(address, coin)
	valid, err := bip32.Base58DecodeCheck(address, networkConstants)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	sendResponse(w, valid)
}

// CreateTransaction create a transaction that can be processed by a device
// POST: /account/createtransaction/{coin}/{xpub}
// Input {"feePerByte": <uint>, "outputs": [{"address": "<string>", "amount": <int>},...]}
func (a *AccountHandler) CreateTransaction(w http.ResponseWriter, r *http.Request) {
	xpub, coin, err := a.checkVars(r)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	account, err := a.accountForCoin(coin, xpub)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	var sendTransactoin bip32.SendTransaction
	err = json.NewDecoder(r.Body).Decode(&sendTransactoin)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	output, err := account.CreateTransaction(&sendTransactoin)
	if err != nil {
		sendError(w, err, http.StatusBadRequest)
		return
	}

	sendResponse(w, output)
}

// Close will close the resources which are being used by this
// handler. This will allow caches and open files to be safely terminated.
func (a *AccountHandler) Close() {
	log.Info("closing resources")
	a.cache.Close()
}

// SetupAccountHandler will return an AccountHandler struct that contains
// references to the various resources this handler will use.
func SetupAccountHandler() *AccountHandler {
	accountHandler := &AccountHandler{
		accounts:    make(map[string][]*bip32.Account),
		refreshLock: make(map[*bip32.Account]*sync.Mutex),
		cache:       settings.CreateKeyValueDBWithFilename("cache.db"),
		providers:   make(map[bip32.NetworkConstants]blockprovider.APIProvider),
	}
	return accountHandler
}
