package devices

import (
	"bufio"
	"fmt"
	"os"
)

type NotifyPinFunc func(needed bool)

type ResponseFunc func(response interface{})

// ConsolePinAck grab the pin from the console
func ConsolePinAck(deviceID string) string {
	pinChan := make(chan string)

	a := func() {
		fmt.Printf("Pin for '%s'> ", deviceID)
		reader := bufio.NewReader(os.Stdin)
		pin, _ := reader.ReadString('\n')
		pin = pin[:len(pin)-1]
		pinChan <- pin
	}
	go a()
	pin := <-pinChan
	return pin
}
