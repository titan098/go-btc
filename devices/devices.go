package devices

import (
	"encoding/binary"
	"encoding/hex"
	"time"

	"bitbucket.org/titan098/go-btc/bip32"
	"bitbucket.org/titan098/go-btc/logging"
	"github.com/gogo/protobuf/proto"
	"github.com/karalabe/hid"
)

var log = logging.SetupLogging("hiddevice")

type HWDevice interface {
	Initialize() error

	Cancel() error

	GetAddress(path []uint32, coinName string, display bool, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error)
	GetEntropy(size uint32) ([]byte, error)
	GetPublicKey(path []uint32, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error)

	SignMessage(path []uint32, message []byte, coin bip32.NetworkConstants, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (*Signature, error)
	VerifyMessage(address string, signature []byte, message []byte, coin bip32.NetworkConstants) (bool, error)
}

type CryptoDevice struct {
	HWDevice
	hidDevice *HIDDevice

	DeviceID string              `json:"deviceid"`
	PinAck   func(string) string `json:"-"`
}

// HIDDevice interface describes the interaction with an api provider
type HIDDevice struct {
	DeviceInfo hid.DeviceInfo
	Device     *hid.Device
	CanCancel  bool
}

// HIDResponse contains the response from the HIDDevice
type HIDResponse struct {
	Data          []byte
	MessageType   uint16
	MessageLength uint32
}

// Signature represents a basic encoded signature returned from the device
type Signature struct {
	Address   string   `json:"address,omitempty"`
	Path      []uint32 `json:"path,omitempty"`
	Signature []byte   `json:"signature,omitempty"`
	Message   []byte   `json:"message,omitempty"`
	Coin      string   `json:"coin,omitempty"`
}

// ReadResponse reads a response from a Keepkey device. The format of the request
// is as follows:
//
// Byte | Size | Description
//    0 | 1    | Size of request, always 63
//    1 | 1    | Header indicator ('#')
//    2 | 1    | Header indicator ('#')
//    3 | 2    | Message Type (Big Endian)
//    5 | 4    | Message Length (Big Endian)
//    9 | X    | protobuf encoded message
func (device *HIDDevice) ReadResponse(blocking bool) (*HIDResponse, error) {
	response := make([]byte, 64)

	bytesRead, err := device.Device.Read(response)
	if err != nil {
		return nil, err
	}

	// if we need to block then block until we have a response
	for (!blocking) && (bytesRead == 0) {
		time.Sleep(100 * time.Millisecond)
		bytesRead, err = device.Device.Read(response)
		if err != nil {
			return nil, err
		}
	}

	messageTypeRead := binary.BigEndian.Uint16(response[3:5])
	messageLengthRead := binary.BigEndian.Uint32(response[5:9])
	totalRead := len(response[9:])

	readBuffer := make([]byte, messageLengthRead)
	copy(readBuffer, response[9:])

	for uint32(totalRead) <= messageLengthRead {
		_, err := device.Device.Read(response)
		if err != nil {
			return nil, err
		}
		copy(readBuffer[totalRead:], response[1:])
		totalRead += len(response[1:])
	}

	responseReturn := &HIDResponse{
		Data:          readBuffer,
		MessageType:   messageTypeRead,
		MessageLength: messageLengthRead,
	}

	log.Debug("recieved message from device:", responseReturn.MessageType)
	device.CanCancel = false

	return responseReturn, nil
}

// WriteRequest write a request to a keepkey device. The format of the request
// is as follows:
//
// Byte | Size | Description
//    0 | 1    | Size of request, always 63
//    1 | 1    | Header indicator ('#')
//    2 | 1    | Header indicator ('#')
//    3 | 2    | Message Type (Big Endian)
//    5 | 4    | Message Length (Big Endian)
//    9 | X    | protobuf encoded message
func (device *HIDDevice) WriteRequest(data []byte, messageType int32) error {
	buffer := make([]byte, 64)
	messageTypeBuffer := make([]byte, 4)
	lengthBuffer := make([]byte, 32)
	totalWritten := 0

	binary.BigEndian.PutUint16(messageTypeBuffer, uint16(messageType))
	binary.BigEndian.PutUint32(lengthBuffer, uint32(len(data)))
	buffer[0] = 63
	copy(buffer[1:], "##")
	copy(buffer[3:], messageTypeBuffer)
	copy(buffer[5:], lengthBuffer)
	copy(buffer[9:], data)

	// write the initial packet and record the total minus the size of the
	// header.
	byteWritten, err := device.Device.Write(buffer)
	totalWritten += byteWritten - 9
	if err != nil {
		return err
	}
	log.Debugf("wrote: %s", hex.EncodeToString(buffer))

	// if there is still data to write then write each of the packets in
	// turn. The packets are written until we write more than the length of the
	// data buffer.
	for totalWritten < len(data) {
		buffer = make([]byte, 64)
		buffer[0] = 63
		copy(buffer[1:], data[totalWritten:])

		byteWritten, err := device.Device.Write(buffer)
		if err != nil {
			return err
		}
		log.Debugf("wrote: %s", hex.EncodeToString(buffer))
		totalWritten += byteWritten - 1
	}

	return nil
}

// DoRequest will send a protobuf serialised message to the KeepKey, and error
// is returned if we could not write to the HID device.
func (device *HIDDevice) DoRequest(request proto.Message, messageType int32) error {
	log.Debug("sending message to device: ", messageType)
	// Marshal the incoming message
	protoData, err := proto.Marshal(request)
	if err != nil {
		return err
	}

	// Write the message to the HID device
	err = device.WriteRequest(protoData, messageType)
	if err != nil {
		return err
	}

	return nil
}

// ProcessResponse will process a raw response from the KeepKey and marshal it
// into the protobuf messages response parameter. An error is returned if
// the object could not be marshalled.
func (device *HIDDevice) ProcessResponse(rawResponse *HIDResponse, response proto.Message) error {
	err := proto.Unmarshal(rawResponse.Data, response)
	if err != nil {
		return err
	}
	return nil
}

// DoResponse will read a response from the KeepKey and marshal into the response
// object. The process can block while waiting for a response from the device.
// An error is returned if the object cannot be marshalled or processed.
func (device *HIDDevice) DoResponse(response proto.Message, blocking bool) error {
	// Read the response from the HID device
	data, err := device.ReadResponse(blocking)
	if err != nil {
		return err
	}

	// Unmarshal the response from the HID device
	err = device.ProcessResponse(data, response)
	if err != nil {
		return err
	}

	return nil
}

// DoRequestResponse will perform doRequest and doResponse in a single function
// call. An error is returned during marshalling and unmarshalling.
func (device *HIDDevice) DoRequestResponse(request proto.Message, response proto.Message, messageType int32) error {
	err := device.DoRequest(request, messageType)
	if err != nil {
		return err
	}

	err = device.DoResponse(response, false)
	if err != nil {
		return err
	}

	return nil
}

func GetDevices(pinAckFunc func(string) string) []*TrezorDevice {
	log.Info("XXXXXXX")
	devices := []*TrezorDevice{}
	devices = append(devices, OpenDeviceTrezor(pinAckFunc)...)
	return devices
}

// func main() {
// 	device := GetDevices(ConsolePinAck)[0]
//
// 	fmt.Println(device.GetFeatures())
// 	coin := device.Features.GetCoins()[1]
// 	fmt.Println(coin)
//
// 	path := []uint32{0, 0, 0}
// 	message := []byte("Hello World")
// 	signature, _ := device.SignMessage(path, message, bip32.TestnetNetwork, nil, nil)
// 	fmt.Printf("Address: %s\n", signature.Address)
// 	fmt.Printf("Message: %s\n", base64.StdEncoding.EncodeToString(message))
// 	fmt.Printf("Signature: %s\n", base64.StdEncoding.EncodeToString(signature.Signature))
//
// 	device.VerifyMessage(signature.Address, signature.Signature, message, bip32.TestnetNetwork)
// 	entropy, _ := device.GetEntropy(32)
// 	fmt.Println(entropy)
// 	address, err := device.GetPublicKey(path, nil, nil)
// 	if err != nil {
// 		fmt.Println(err.Error())
// 	}
// 	fmt.Println(address)
//
// 	device.GetAddress(path, "Testnet", true, nil, nil)
// }
