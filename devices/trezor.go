package devices

import (
	"encoding/hex"
	"errors"

	"github.com/karalabe/hid"

	"bitbucket.org/titan098/go-btc/bip32"
	"bitbucket.org/titan098/go-btc/blockprovider"
	"bitbucket.org/titan098/go-btc/devices/trezorproto"
)

const (
	vendorID  = 0x534c
	productID = 0x0001
)

// TrezorDevice represents the base structure for a valid KeepKey USB device.
type TrezorDevice struct {
	CryptoDevice
	Features *trezorproto.Features `json:"features,omitempty"`
}

// ButtonAck will acknowledge and block for the user to push the button.
func (trezor *TrezorDevice) ButtonAck() error {
	request := &trezorproto.ButtonAck{}
	messageType := trezorproto.MessageType_value["MessageType_ButtonAck"]
	trezor.hidDevice.CanCancel = true
	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return err
	}

	return nil
}

// parseFailure will parse a Failure object that is returned from the device
func (trezor *TrezorDevice) parseFailure(rawResponse *HIDResponse) (*trezorproto.Failure, error) {
	response := &trezorproto.Failure{}
	err := trezor.hidDevice.ProcessResponse(rawResponse, response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// DoPinAcknowledgement will send an acknowledged pin to the device.
// An error is returned if the user did not enter a valid pin.
func (trezor *TrezorDevice) DoPinAcknowledgement() error {
	pin := trezor.PinAck(trezor.DeviceID)
	request := &trezorproto.PinMatrixAck{
		Pin: &pin,
	}
	messageType := trezorproto.MessageType_value["MessageType_PinMatrixAck"]

	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return err
	}

	return nil
}

func (trezor *TrezorDevice) handlePinAcknowledgement(notifyPinFunc NotifyPinFunc) error {
	// did we get a request to validate the pin
	if notifyPinFunc != nil {
		notifyPinFunc(true)
	}
	err := trezor.DoPinAcknowledgement()
	if err != nil {
		return err
	}
	return nil
}

// GetEntropy fetches entropy and returns a byte array. An error is returned
// if the entropy cannot be returned
func (trezor *TrezorDevice) GetEntropy(size uint32) ([]byte, error) {
	request := &trezorproto.GetEntropy{
		Size: &size,
	}
	messageType := trezorproto.MessageType_value["MessageType_GetEntropy"]

	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return nil, err
	}

	done := false
	blocking := false
	for !done {
		rawResponse, err := trezor.hidDevice.ReadResponse(blocking)
		blocking = false
		if err != nil {
			return nil, err
		}

		switch trezorproto.MessageType(rawResponse.MessageType) {
		case trezorproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			trezor.ButtonAck()
			blocking = true

		case trezorproto.MessageType_MessageType_Entropy:
			// did we get some entropy
			response := &trezorproto.Entropy{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			done = true
			return response.GetEntropy(), nil

		default:
			return nil, errors.New("Unknown message type for Entropy")
		}
	}

	return nil, nil
}

// Cancel will instruct the KeepKeyDevice to send a Cancel message.
// Actions which require user interaction can be canceled.
func (trezor *TrezorDevice) Cancel() error {
	log.Debug("sending cancel to device")
	request := &trezorproto.Cancel{}
	messageType := trezorproto.MessageType_value["MessageType_Cancel"]

	if trezor.hidDevice.CanCancel {
		log.Debug("cancelling current interaction")
		err := trezor.hidDevice.DoRequest(request, messageType)
		if err != nil {
			return err
		}
	} else {
		log.Debug("there was nothing to cancel")
	}
	return nil
}

// GetAddress will returne the Base58 encoded address for the given path.
// If the display is true then the device will block for the user to acknowledge
// the address through a button push.
func (trezor *TrezorDevice) GetAddress(path []uint32, coinName string, display bool, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error) {
	request := &trezorproto.GetAddress{
		AddressN:    path,
		CoinName:    &coinName,
		ShowDisplay: &display,
	}
	messageType := trezorproto.MessageType_MessageType_GetAddress

	// do the request
	err := trezor.hidDevice.DoRequest(request, int32(messageType))
	if err != nil {
		return "", err
	}

	done := false
	blocking := false
	notified := false
	for !done {
		// read the response, we will decide what to do based on the message returned
		rawResponse, err := trezor.hidDevice.ReadResponse(blocking)
		blocking = false
		if err != nil {
			return "", err
		}

		switch trezorproto.MessageType(rawResponse.MessageType) {
		case trezorproto.MessageType_MessageType_PinMatrixRequest:
			err := trezor.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return "", err
			}

		case trezorproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			trezor.ButtonAck()
			blocking = true

		case trezorproto.MessageType_MessageType_Address:
			// did we get an address
			response := &trezorproto.Address{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			address := response.GetAddress()
			done = true

			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				responseFunc(address)
			}

			return address, nil

		case trezorproto.MessageType_MessageType_Failure:
			// did we get a failure?
			response := &trezorproto.Failure{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			log.Errorf("getpublickey failure: %d %s", response.GetCode(), response.GetMessage())
			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(errors.New(response.GetMessage()))
			}
			return "", errors.New(response.GetMessage())

		default:
			return "", errors.New("unknown message type for address")
		}
	}

	return "", nil
}

// GetPublicKey will return the xpub seralised public key for the given path.
// An error will be returned if the message cannot be seralised.
func (trezor *TrezorDevice) GetPublicKey(path []uint32, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error) {
	request := &trezorproto.GetPublicKey{
		AddressN: path,
	}
	messageType := trezorproto.MessageType_value["MessageType_GetPublicKey"]

	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return "", err
	}

	done := false
	notified := false
	for !done {
		rawResponse, err := trezor.hidDevice.ReadResponse(false)
		if err != nil {
			return "", err
		}

		switch trezorproto.MessageType(rawResponse.MessageType) {
		case trezorproto.MessageType_MessageType_PinMatrixRequest:
			err := trezor.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return "", err
			}

		case trezorproto.MessageType_MessageType_PublicKey:
			// did we get the public key
			response := &trezorproto.PublicKey{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			xpub := response.GetXpub()

			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				responseFunc(xpub)
			}
			return xpub, nil

		case trezorproto.MessageType_MessageType_Failure:
			// did we get a failure?
			response := &trezorproto.Failure{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			log.Errorf("getpublickey failure: %d %s", response.GetCode(), response.GetMessage())
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(errors.New(response.GetMessage()))
			}

			return "", errors.New(response.GetMessage())

		default:
			return "", errors.New("Unknown message type for GetPublicKey")
		}
	}

	return "", nil
}

// GetFeatures returnes the feature set of the device, a Features object is returned
// or an error there was an error processing the result.
func (trezor *TrezorDevice) GetFeatures() (*trezorproto.Features, error) {
	request := &trezorproto.GetFeatures{}
	messageType := trezorproto.MessageType_value["MessageType_GetFeatures"]
	response := &trezorproto.Features{}

	err := trezor.hidDevice.DoRequestResponse(request, response, messageType)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (trezor *TrezorDevice) SignTx(outputTransaction *bip32.OutputTransaction, coin bip32.NetworkConstants, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error) {
	messageType := trezorproto.MessageType_value["MessageType_SignTx"]
	inputCount := uint32(len(outputTransaction.Vin))
	outputCount := uint32(len(outputTransaction.Vout))

	rawTx := map[string]*blockprovider.TxIn{}

	request := &trezorproto.SignTx{
		InputsCount:  &inputCount,
		OutputsCount: &outputCount,
		CoinName:     &coin.Name,
	}

	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return "", err
	}

	done := false
	blocking := false
	notified := false
	var serializedTx []byte
	for !done {
		rawResponse, err := trezor.hidDevice.ReadResponse(blocking)
		blocking = false
		if err != nil {
			return "", err
		}

		switch trezorproto.MessageType(rawResponse.MessageType) {
		case trezorproto.MessageType_MessageType_PinMatrixRequest:
			err := trezor.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return "", err
			}

		case trezorproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			trezor.ButtonAck()
			blocking = true

		case trezorproto.MessageType_MessageType_Failure:
			response := &trezorproto.Failure{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			log.Errorf("signtx failure: %d %s", response.GetCode(), response.GetMessage())
			done = true

			//notify any listeners of the failure
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(response.GetMessage())
			}

			return "", errors.New(response.GetMessage())

		case trezorproto.MessageType_MessageType_TxRequest:
			response := &trezorproto.TxRequest{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			log.Debugf("txrequest: type %s (%s)\n", trezorproto.RequestType_name[int32(response.GetRequestType())], response.GetDetails())

			if response.GetSerialized().GetSerializedTx() != nil {
				tx := response.GetSerialized().GetSerializedTx()
				log.Debug("got part of a serialized tx: ", hex.EncodeToString(tx))
				serializedTx = append(serializedTx, tx...)
			}

			switch response.GetRequestType() {
			case trezorproto.RequestType_TXINPUT:
				var vinIndex = response.GetDetails().GetRequestIndex()
				var txIn = outputTransaction.Vin[vinIndex]
				rawTx[txIn.PrevHash] = &txIn
				inputs := []*trezorproto.TxInputType{}
				prevHash, _ := hex.DecodeString(txIn.PrevHash)
				script, _ := hex.DecodeString(txIn.Script)
				prevIndex := uint32(txIn.PrevIndex)

				input := &trezorproto.TxInputType{
					AddressN:  txIn.AddressN,
					PrevHash:  prevHash,
					PrevIndex: &prevIndex,
					ScriptSig: script,
					Amount:    &txIn.Amount,
				}
				inputs = append(inputs, input)

				request := &trezorproto.TxAck{
					Tx: &trezorproto.TransactionType{
						Inputs: inputs,
					},
				}
				messageType := trezorproto.MessageType_value["MessageType_TxAck"]
				err := trezor.hidDevice.DoRequest(request, messageType)
				if err != nil {
					return "", err
				}

			case trezorproto.RequestType_TXOUTPUT:
				var vinIndex = response.GetDetails().GetRequestIndex()
				var txOut = outputTransaction.Vout[vinIndex]
				outputs := []*trezorproto.TxOutputType{}
				scriptType := trezorproto.OutputScriptType_PAYTOADDRESS

				output := &trezorproto.TxOutputType{
					Address:    &txOut.Address,
					Amount:     &txOut.Amount,
					ScriptType: &scriptType,
				}

				if txOut.Change {
					output.AddressN = txOut.AddressN
				} else {
					output.Address = &txOut.Address
				}

				outputs = append(outputs, output)

				request := &trezorproto.TxAck{
					Tx: &trezorproto.TransactionType{
						Outputs: outputs,
					},
				}
				messageType := trezorproto.MessageType_value["MessageType_TxAck"]
				err := trezor.hidDevice.DoRequest(request, messageType)
				if err != nil {
					return "", err
				}

			case trezorproto.RequestType_TXFINISHED:
				log.Debugf("final transaction: %s\n", hex.EncodeToString(serializedTx))
				done = true
				transaction := hex.EncodeToString(serializedTx)

				//send the transaction to any listeners
				if notifyPinFunc != nil && !notified {
					notifyPinFunc(false)
				}
				if responseFunc != nil {
					responseFunc(transaction)
				}

				return transaction, nil

			default:
				log.Error("unknown txrequest type: ", response.GetRequestType())
				done = true
				return "", errors.New("Unknown message type for TxRequest")
			}

		default:
			return "", errors.New("Unknown message type for SignTx")
		}
	}

	return "", nil
}

// VerifyMessage will verify a message signature for a bitcoin address. True will
// be returned if the message is value. False and an error will be returned
// if the message cannot be verified.
func (trezor *TrezorDevice) VerifyMessage(address string, signature []byte, message []byte, coin bip32.NetworkConstants) (bool, error) {
	var request = &trezorproto.VerifyMessage{
		Address:   &address,
		Signature: signature,
		Message:   message,
		CoinName:  &coin.Name,
	}
	var messageType = trezorproto.MessageType_value["MessageType_VerifyMessage"]

	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return false, err
	}

	done := false
	blocking := false
	for !done {
		// read the response, we will decide what to do based on the message returned
		rawResponse, err := trezor.hidDevice.ReadResponse(blocking)
		blocking = false
		if err != nil {
			return false, err
		}

		switch trezorproto.MessageType(rawResponse.MessageType) {
		case trezorproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			trezor.ButtonAck()
			blocking = true

		case trezorproto.MessageType_MessageType_Success:
			//did we get a message signature
			var response = &trezorproto.Success{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			done = true
			return true, nil

		case trezorproto.MessageType_MessageType_Failure:
			//did we get a message signature
			var response = &trezorproto.Failure{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			done = true
			log.Errorf("keepkey failure: %d %s", response.GetCode(), response.GetMessage())
			return false, errors.New(response.GetMessage())

		default:
			return false, errors.New("Unknown message type for SignMessage")
		}
	}

	return false, nil
}

// SignMessage will sign a message given the path in the wallet. An error will be
// returned if the message cannot be signed.
func (trezor *TrezorDevice) SignMessage(path []uint32, message []byte, coin bip32.NetworkConstants, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (*Signature, error) {
	var request = &trezorproto.SignMessage{
		AddressN: path,
		Message:  message,
		CoinName: &coin.Name,
	}
	var messageType = trezorproto.MessageType_value["MessageType_SignMessage"]
	//var response = &trezorproto.MessageSignature{}

	err := trezor.hidDevice.DoRequest(request, messageType)
	if err != nil {
		return nil, err
	}

	done := false
	notified := false
	blocking := false
	for !done {
		// read the response, we will decide what to do based on the message returned
		rawResponse, err := trezor.hidDevice.ReadResponse(blocking)
		blocking = false
		if err != nil {
			return nil, err
		}

		switch trezorproto.MessageType(rawResponse.MessageType) {
		case trezorproto.MessageType_MessageType_PinMatrixRequest:
			err := trezor.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return nil, err
			}

		case trezorproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			trezor.ButtonAck()
			blocking = true

		case trezorproto.MessageType_MessageType_MessageSignature:
			//did we get a message signature
			var response = &trezorproto.MessageSignature{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			done = true
			signature := &Signature{
				Address:   response.GetAddress(),
				Signature: response.GetSignature(),
				Message:   message,
			}

			//notify the listner functions
			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				responseFunc(signature)
			}

			return signature, nil

		case trezorproto.MessageType_MessageType_Failure:
			// did we get a failure?
			response := &trezorproto.Failure{}
			trezor.hidDevice.ProcessResponse(rawResponse, response)
			log.Errorf("signmessage failure: %d %s", response.GetCode(), response.GetMessage())
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(response.GetMessage())
			}

			return nil, errors.New(response.GetMessage())

		default:
			return nil, errors.New("Unknown message type for SignMessage")
		}
	}

	return nil, nil
}

// Initialize request that the device is Initialized to a start state.
// An error is returned if the device cannot be initialised.
func (trezor *TrezorDevice) Initialize() error {
	log.Debug("initializing trezor: ", trezor.hidDevice.Device.Path)

	if trezor.hidDevice.DeviceInfo.UsagePage == 0xf1d0 {
		return errors.New("this devices is the u2f interface")
	}

	request := &trezorproto.Initialize{}
	messageType := trezorproto.MessageType_value["MessageType_Initialize"]
	response := &trezorproto.Features{}

	err := trezor.hidDevice.DoRequestResponse(request, response, messageType)
	if err != nil {
		return err
	}

	trezor.Features = response
	trezor.DeviceID = response.GetDeviceId()
	log.Infof("trezor device found '%s (%s)'", trezor.Features.GetLabel(), trezor.Features.GetDeviceId())
	return nil
}

// OpenDeviceTrezor will enumerate the USB Trezor devices which are available on the
// system. An array of TrezorDevice is returned for each valid device.
func OpenDeviceTrezor(pinAckFunc func(string) string) []*TrezorDevice {
	log.Debug("fetching trezor devices")
	trezorDevices := []*TrezorDevice{}
	hidDevices := hid.Enumerate(vendorID, productID)
	log.Debug("%v", hidDevices)
	log.Debug("number of trezor devices found:", len(hidDevices))
	for _, newHidDevice := range hidDevices {

		// try to open the hid device
		openDevice, err := newHidDevice.Open()
		if err == nil {
			newTrezorDevice := &TrezorDevice{
				CryptoDevice: CryptoDevice{
					hidDevice: &HIDDevice{
						DeviceInfo: newHidDevice,
						Device:     openDevice,
						CanCancel:  false,
					},
				},
			}

			hidErr := newTrezorDevice.Initialize()
			if hidErr == nil {
				newTrezorDevice.PinAck = pinAckFunc
				trezorDevices = append(trezorDevices, newTrezorDevice)
			} else {
				log.Error(hidErr.Error())
			}
		} else {
			log.Error("trezor device could not be opened: ", err.Error())
		}
	}
	return trezorDevices
}
