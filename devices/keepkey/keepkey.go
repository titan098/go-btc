package keepkey

import (
	"bufio"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"time"

	"bitbucket.org/titan098/go-btc/bip32"
	"bitbucket.org/titan098/go-btc/blockprovider"
	"bitbucket.org/titan098/go-btc/keepkeyproto"
	"bitbucket.org/titan098/go-btc/logging"

	"github.com/golang/protobuf/proto"
	"github.com/karalabe/hid"
)

var log = logging.SetupLogging("keepkey")

const (
	vendorID  = 0x2b24
	productID = 0x0001
)

// KeepKeyDevice represents the base structure for a valid KeepKey USB device.
type KeepKeyDevice struct {
	deviceInfo hid.DeviceInfo
	device     *hid.Device
	canCancel  bool

	DeviceID string                 `json:"deviceid"`
	Features *keepkeyproto.Features `json:"features,omitempty"`
	PinAck   func(string) string    `json:"-"`
}

// Signature represents a basic encoded signature returned from the device
type Signature struct {
	Address   string   `json:"address,omitempty"`
	Path      []uint32 `json:"path,omitempty"`
	Signature []byte   `json:"signature,omitempty"`
	Message   []byte   `json:"message,omitempty"`
	Coin      string   `json:"coin,omitempty"`
}

type hidResponse struct {
	Data          []byte
	MessageType   keepkeyproto.MessageType
	MessageLength uint32
}

type NotifyPinFunc func(needed bool)

type ResponseFunc func(response interface{})

// readResponse reads a response from a Keepkey device. The format of the request
// is as follows:
//
// Byte | Size | Description
//    0 | 1    | Size of request, always 63
//    1 | 1    | Header indicator ('#')
//    2 | 1    | Header indicator ('#')
//    3 | 2    | Message Type (Big Endian)
//    5 | 4    | Message Length (Big Endian)
//    9 | X    | protobuf encoded message
func (keepKey *KeepKeyDevice) readResponse(blocking bool) (*hidResponse, error) {
	response := make([]byte, 64)

	bytesRead, err := keepKey.device.Read(response)
	if err != nil {
		return nil, err
	}

	// if we need to block then block until we have a response
	for (!blocking) && (bytesRead == 0) {
		time.Sleep(100 * time.Millisecond)
		bytesRead, err = keepKey.device.Read(response)
		if err != nil {
			return nil, err
		}
	}

	messageTypeRead := binary.BigEndian.Uint16(response[3:5])
	messageLengthRead := binary.BigEndian.Uint32(response[5:9])
	totalRead := len(response[9:])

	readBuffer := make([]byte, messageLengthRead)
	copy(readBuffer, response[9:])

	for uint32(totalRead) <= messageLengthRead {
		_, err := keepKey.device.Read(response)
		if err != nil {
			return nil, err
		}
		copy(readBuffer[totalRead:], response[1:])
		totalRead += len(response[1:])
	}

	responseReturn := &hidResponse{
		Data:          readBuffer,
		MessageType:   keepkeyproto.MessageType(messageTypeRead),
		MessageLength: messageLengthRead,
	}

	log.Debug("recieved message from keepkey:", keepkeyproto.MessageType_name[int32(responseReturn.MessageType)])
	keepKey.canCancel = false

	return responseReturn, nil
}

// writeRequest write a request to a keepkey device. The format of the request
// is as follows:
//
// Byte | Size | Description
//    0 | 1    | Size of request, always 63
//    1 | 1    | Header indicator ('#')
//    2 | 1    | Header indicator ('#')
//    3 | 2    | Message Type (Big Endian)
//    5 | 4    | Message Length (Big Endian)
//    9 | X    | protobuf encoded message
func (keepKey *KeepKeyDevice) writeRequest(data []byte, messageType int32) error {
	buffer := make([]byte, 64)
	messageTypeBuffer := make([]byte, 4)
	lengthBuffer := make([]byte, 32)
	totalWritten := 0

	binary.BigEndian.PutUint16(messageTypeBuffer, uint16(messageType))
	binary.BigEndian.PutUint32(lengthBuffer, uint32(len(data)))
	buffer[0] = 63
	copy(buffer[1:], "##")
	copy(buffer[3:], messageTypeBuffer)
	copy(buffer[5:], lengthBuffer)
	copy(buffer[9:], data)

	// write the initial packet and record the total minus the size of the
	// header.
	byteWritten, err := keepKey.device.Write(buffer)
	totalWritten += byteWritten - 9
	if err != nil {
		return err
	}
	log.Debugf("wrote: %s", hex.EncodeToString(buffer))

	// if there is still data to write then write each of the packets in
	// turn. The packets are written until we write more than the length of the
	// data buffer.
	for totalWritten < len(data) {
		buffer = make([]byte, 64)
		buffer[0] = 63
		copy(buffer[1:], data[totalWritten:])

		byteWritten, err := keepKey.device.Write(buffer)
		if err != nil {
			return err
		}
		log.Debugf("wrote: %s", hex.EncodeToString(buffer))
		totalWritten += byteWritten - 1
	}

	return nil
}

// doRequest will send a protobuf serialised message to the KeepKey, and error
// is returned if we could not write to the HID device.
func (keepKey *KeepKeyDevice) doRequest(request proto.Message, messageType int32) error {
	log.Debug("sending message to keepkey: ", keepkeyproto.MessageType_name[messageType])
	// Marshal the incoming message
	protoData, err := proto.Marshal(request)
	if err != nil {
		return err
	}

	// Write the message to the HID device
	err = keepKey.writeRequest(protoData, messageType)
	if err != nil {
		return err
	}

	return nil
}

// processResponse will process a raw response from the KeepKey and marshal it
// into the protobuf messages response parameter. An error is returned if
// the object could not be marshalled.
func (keepKey *KeepKeyDevice) processResponse(rawResponse *hidResponse, response proto.Message) error {
	err := proto.Unmarshal(rawResponse.Data, response)
	if err != nil {
		return err
	}
	return nil
}

// doReponse will read a response from the KeepKey and marshal into the response
// object. The process can block while waiting for a response from the device.
// An error is returned if the object cannot be marshalled or processed.
func (keepKey *KeepKeyDevice) doResponse(response proto.Message, blocking bool) error {
	// Read the response from the HID device
	data, err := keepKey.readResponse(blocking)
	if err != nil {
		return err
	}

	// Unmarshal the response from the HID device
	err = keepKey.processResponse(data, response)
	if err != nil {
		return err
	}

	return nil
}

// doRequestResponse will perform doRequest and doResponse in a single function
// call. An error is returned during marshalling and unmarshalling.
func (keepKey *KeepKeyDevice) doRequestResponse(request proto.Message, response proto.Message, messageType int32) error {
	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return err
	}

	err = keepKey.doResponse(response, false)
	if err != nil {
		return err
	}

	return nil
}

// ButtonAck will acknowledge and block for the user to push the button.
func (keepKey *KeepKeyDevice) ButtonAck() error {
	request := &keepkeyproto.ButtonAck{}
	messageType := keepkeyproto.MessageType_value["MessageType_ButtonAck"]
	keepKey.canCancel = true
	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return err
	}

	return nil
}

// parseFailure will parse a Failure object that is returned from the device
func (keepKey *KeepKeyDevice) parseFailure(rawResponse *hidResponse) (*keepkeyproto.Failure, error) {
	response := &keepkeyproto.Failure{}
	err := keepKey.processResponse(rawResponse, response)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// DoPinAcknowledgement will send an acknowledged pin to the device.
// An error is returned if the user did not enter a valid pin.
func (keepKey *KeepKeyDevice) DoPinAcknowledgement() error {
	pin := keepKey.PinAck(keepKey.DeviceID)
	request := &keepkeyproto.PinMatrixAck{
		Pin: &pin,
	}
	messageType := keepkeyproto.MessageType_value["MessageType_PinMatrixAck"]

	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return err
	}

	return nil
}

func (keepKey *KeepKeyDevice) handlePinAcknowledgement(notifyPinFunc NotifyPinFunc) error {
	// did we get a request to validate the pin
	if notifyPinFunc != nil {
		notifyPinFunc(true)
	}
	err := keepKey.DoPinAcknowledgement()
	if err != nil {
		return err
	}
	return nil
}

// GetEntropy fetches entropy and returns a byte array. An error is returned
// if the entropy cannot be returned
func (keepKey *KeepKeyDevice) GetEntropy(size uint32) ([]byte, error) {
	request := &keepkeyproto.GetEntropy{
		Size: &size,
	}
	messageType := keepkeyproto.MessageType_value["MessageType_GetEntropy"]

	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return nil, err
	}

	done := false
	blocking := false
	for !done {
		rawResponse, err := keepKey.readResponse(blocking)
		blocking = false
		if err != nil {
			return nil, err
		}

		switch rawResponse.MessageType {
		case keepkeyproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			keepKey.ButtonAck()
			blocking = true

		case keepkeyproto.MessageType_MessageType_Entropy:
			// did we get some entropy
			response := &keepkeyproto.Entropy{}
			keepKey.processResponse(rawResponse, response)
			done = true
			return response.GetEntropy(), nil

		default:
			return nil, errors.New("Unknown message type for Entropy")
		}
	}

	return nil, nil
}

// Cancel will instruct the KeepKeyDevice to send a Cancel message.
// Actions which require user interaction can be canceled.
func (keepKey *KeepKeyDevice) Cancel() error {
	log.Debug("sending cancel to device")
	request := &keepkeyproto.Cancel{}
	messageType := keepkeyproto.MessageType_value["MessageType_Cancel"]

	if keepKey.canCancel {
		log.Debug("cancelling current interaction")
		err := keepKey.doRequest(request, messageType)
		if err != nil {
			return err
		}
	} else {
		log.Debug("there was nothing to cancel")
	}
	return nil
}

// GetAddress will returne the Base58 encoded address for the given path.
// If the display is true then the device will block for the user to acknowledge
// the address through a button push.
func (keepKey *KeepKeyDevice) GetAddress(path []uint32, coinName string, display bool, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error) {
	request := &keepkeyproto.GetAddress{
		AddressN:    path,
		CoinName:    &coinName,
		ShowDisplay: &display,
	}
	messageType := keepkeyproto.MessageType_MessageType_GetAddress

	// do the request
	err := keepKey.doRequest(request, int32(messageType))
	if err != nil {
		return "", err
	}

	done := false
	blocking := false
	notified := false
	for !done {
		// read the response, we will decide what to do based on the message returned
		rawResponse, err := keepKey.readResponse(blocking)
		blocking = false
		if err != nil {
			return "", err
		}

		switch rawResponse.MessageType {
		case keepkeyproto.MessageType_MessageType_PinMatrixRequest:
			err := keepKey.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return "", err
			}

		case keepkeyproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			keepKey.ButtonAck()
			blocking = true

		case keepkeyproto.MessageType_MessageType_Address:
			// did we get an address
			response := &keepkeyproto.Address{}
			keepKey.processResponse(rawResponse, response)
			address := response.GetAddress()
			done = true

			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				responseFunc(address)
			}

			return address, nil

		case keepkeyproto.MessageType_MessageType_Failure:
			// did we get a failure?
			response := &keepkeyproto.Failure{}
			keepKey.processResponse(rawResponse, response)
			log.Errorf("getpublickey failure: %d %s", response.GetCode(), response.GetMessage())
			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(errors.New(response.GetMessage()))
			}
			return "", errors.New(response.GetMessage())

		default:
			return "", errors.New("unknown message type for address")
		}
	}

	return "", nil
}

// GetPublicKey will return the xpub seralised public key for the given path.
// An error will be returned if the message cannot be seralised.
func (keepKey *KeepKeyDevice) GetPublicKey(path []uint32, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error) {
	request := &keepkeyproto.GetPublicKey{
		AddressN: path,
	}
	messageType := keepkeyproto.MessageType_value["MessageType_GetPublicKey"]

	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return "", err
	}

	done := false
	notified := false
	for !done {
		rawResponse, err := keepKey.readResponse(false)
		if err != nil {
			return "", err
		}

		switch rawResponse.MessageType {
		case keepkeyproto.MessageType_MessageType_PinMatrixRequest:
			err := keepKey.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return "", err
			}

		case keepkeyproto.MessageType_MessageType_PublicKey:
			// did we get the public key
			response := &keepkeyproto.PublicKey{}
			keepKey.processResponse(rawResponse, response)
			xpub := response.GetXpub()

			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				responseFunc(xpub)
			}
			return xpub, nil

		case keepkeyproto.MessageType_MessageType_Failure:
			// did we get a failure?
			response := &keepkeyproto.Failure{}
			keepKey.processResponse(rawResponse, response)
			log.Errorf("getpublickey failure: %d %s", response.GetCode(), response.GetMessage())
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(errors.New(response.GetMessage()))
			}

			return "", errors.New(response.GetMessage())

		default:
			return "", errors.New("Unknown message type for GetPublicKey")
		}
	}

	return "", nil
}

// GetFeatures returnes the feature set of the device, a Features object is returned
// or an error there was an error processing the result.
func (keepKey *KeepKeyDevice) GetFeatures() (*keepkeyproto.Features, error) {
	request := &keepkeyproto.GetFeatures{}
	messageType := keepkeyproto.MessageType_value["MessageType_GetFeatures"]
	response := &keepkeyproto.Features{}

	err := keepKey.doRequestResponse(request, response, messageType)
	if err != nil {
		return nil, err
	}

	return response, nil
}

func (keepKey *KeepKeyDevice) SignTx(outputTransaction *bip32.OutputTransaction, coin bip32.NetworkConstants, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (string, error) {
	messageType := keepkeyproto.MessageType_value["MessageType_SignTx"]
	inputCount := uint32(len(outputTransaction.Vin))
	outputCount := uint32(len(outputTransaction.Vout))

	rawTx := map[string]*blockprovider.TxIn{}

	request := &keepkeyproto.SignTx{
		InputsCount:  &inputCount,
		OutputsCount: &outputCount,
		CoinName:     &coin.Name,
	}

	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return "", err
	}

	done := false
	blocking := false
	notified := false
	var serializedTx []byte
	for !done {
		rawResponse, err := keepKey.readResponse(blocking)
		blocking = false
		if err != nil {
			return "", err
		}

		switch rawResponse.MessageType {
		case keepkeyproto.MessageType_MessageType_PinMatrixRequest:
			err := keepKey.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return "", err
			}

		case keepkeyproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			keepKey.ButtonAck()
			blocking = true

		case keepkeyproto.MessageType_MessageType_Failure:
			response := &keepkeyproto.Failure{}
			keepKey.processResponse(rawResponse, response)
			log.Errorf("signtx failure: %d %s", response.GetCode(), response.GetMessage())
			done = true

			//notify any listeners of the failure
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(response.GetMessage())
			}

			return "", errors.New(response.GetMessage())

		case keepkeyproto.MessageType_MessageType_TxRequest:
			response := &keepkeyproto.TxRequest{}
			keepKey.processResponse(rawResponse, response)
			log.Debugf("txrequest: type %s (%s)\n", keepkeyproto.RequestType_name[int32(response.GetRequestType())], response.GetDetails())

			if response.GetSerialized().GetSerializedTx() != nil {
				tx := response.GetSerialized().GetSerializedTx()
				log.Debug("got part of a serialized tx: ", hex.EncodeToString(tx))
				serializedTx = append(serializedTx, tx...)
			}

			switch response.GetRequestType() {
			case keepkeyproto.RequestType_TXINPUT:
				var vinIndex = response.GetDetails().GetRequestIndex()
				var txIn = outputTransaction.Vin[vinIndex]
				rawTx[txIn.PrevHash] = &txIn
				inputs := []*keepkeyproto.TxInputType{}
				prevHash, _ := hex.DecodeString(txIn.PrevHash)
				script, _ := hex.DecodeString(txIn.Script)
				prevIndex := uint32(txIn.PrevIndex)

				input := &keepkeyproto.TxInputType{
					AddressN:  txIn.AddressN,
					PrevHash:  prevHash,
					PrevIndex: &prevIndex,
					ScriptSig: script,
					Amount:    &txIn.Amount,
				}
				inputs = append(inputs, input)

				request := &keepkeyproto.TxAck{
					Tx: &keepkeyproto.TransactionType{
						Inputs: inputs,
					},
				}
				messageType := keepkeyproto.MessageType_value["MessageType_TxAck"]
				err := keepKey.doRequest(request, messageType)
				if err != nil {
					return "", err
				}

			case keepkeyproto.RequestType_TXMETA:
				rawTxData := rawTx[hex.EncodeToString(response.GetDetails().GetTxHash())].RawTx
				rawTxBytes, _ := hex.DecodeString(rawTxData)
				request := &keepkeyproto.RawTxAck{
					Tx: &keepkeyproto.RawTransactionType{
						Payload: rawTxBytes,
					},
				}
				messageType := keepkeyproto.MessageType_value["MessageType_RawTxAck"]
				err := keepKey.doRequest(request, messageType)
				if err != nil {
					return "", err
				}

			case keepkeyproto.RequestType_TXOUTPUT:
				var vinIndex = response.GetDetails().GetRequestIndex()
				var txOut = outputTransaction.Vout[vinIndex]
				outputs := []*keepkeyproto.TxOutputType{}
				scriptType := keepkeyproto.OutputScriptType_PAYTOADDRESS

				output := &keepkeyproto.TxOutputType{
					Address:    &txOut.Address,
					Amount:     &txOut.Amount,
					ScriptType: &scriptType,
				}

				var outputType keepkeyproto.OutputAddressType
				if txOut.Change {
					outputType = keepkeyproto.OutputAddressType_CHANGE
					output.AddressN = txOut.AddressN
				} else {
					outputType = keepkeyproto.OutputAddressType_SPEND
					output.Address = &txOut.Address
				}

				output.AddressType = &outputType
				outputs = append(outputs, output)

				request := &keepkeyproto.TxAck{
					Tx: &keepkeyproto.TransactionType{
						Outputs: outputs,
					},
				}
				messageType := keepkeyproto.MessageType_value["MessageType_TxAck"]
				err := keepKey.doRequest(request, messageType)
				if err != nil {
					return "", err
				}

			case keepkeyproto.RequestType_TXFINISHED:
				log.Debugf("final transaction: %s\n", hex.EncodeToString(serializedTx))
				done = true
				transaction := hex.EncodeToString(serializedTx)

				//send the transaction to any listeners
				if notifyPinFunc != nil && !notified {
					notifyPinFunc(false)
				}
				if responseFunc != nil {
					responseFunc(transaction)
				}

				return transaction, nil

			default:
				log.Error("unknown txrequest type: ", response.GetRequestType())
				done = true
				return "", errors.New("Unknown message type for TxRequest")
			}

		default:
			return "", errors.New("Unknown message type for SignTx")
		}
	}

	return "", nil
}

// VerifyMessage will verify a message signature for a bitcoin address. True will
// be returned if the message is value. False and an error will be returned
// if the message cannot be verified.
func (keepKey *KeepKeyDevice) VerifyMessage(address string, signature []byte, message []byte, coin bip32.NetworkConstants) (bool, error) {
	var request = &keepkeyproto.VerifyMessage{
		Address:   &address,
		Signature: signature,
		Message:   message,
		CoinName:  &coin.Name,
	}
	var messageType = keepkeyproto.MessageType_value["MessageType_VerifyMessage"]

	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return false, err
	}

	done := false
	blocking := false
	for !done {
		// read the response, we will decide what to do based on the message returned
		rawResponse, err := keepKey.readResponse(blocking)
		blocking = false
		if err != nil {
			return false, err
		}

		switch rawResponse.MessageType {
		case keepkeyproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			keepKey.ButtonAck()
			blocking = true

		case keepkeyproto.MessageType_MessageType_Success:
			//did we get a message signature
			var response = &keepkeyproto.Success{}
			keepKey.processResponse(rawResponse, response)
			done = true
			return true, nil

		case keepkeyproto.MessageType_MessageType_Failure:
			//did we get a message signature
			var response = &keepkeyproto.Failure{}
			keepKey.processResponse(rawResponse, response)
			done = true
			log.Errorf("keepkey failure: %d %s", response.GetCode(), response.GetMessage())
			return false, errors.New(response.GetMessage())

		default:
			return false, errors.New("Unknown message type for SignMessage")
		}
	}

	return false, nil
}

// SignMessage will sign a message given the path in the wallet. An error will be
// returned if the message cannot be signed.
func (keepKey *KeepKeyDevice) SignMessage(path []uint32, message []byte, coin bip32.NetworkConstants, notifyPinFunc NotifyPinFunc, responseFunc ResponseFunc) (*Signature, error) {
	var request = &keepkeyproto.SignMessage{
		AddressN: path,
		Message:  message,
		CoinName: &coin.Name,
	}
	var messageType = keepkeyproto.MessageType_value["MessageType_SignMessage"]
	//var response = &keepkeyproto.MessageSignature{}

	err := keepKey.doRequest(request, messageType)
	if err != nil {
		return nil, err
	}

	done := false
	notified := false
	blocking := false
	for !done {
		// read the response, we will decide what to do based on the message returned
		rawResponse, err := keepKey.readResponse(blocking)
		blocking = false
		if err != nil {
			return nil, err
		}

		switch rawResponse.MessageType {
		case keepkeyproto.MessageType_MessageType_PinMatrixRequest:
			err := keepKey.handlePinAcknowledgement(notifyPinFunc)
			notified = true
			if err != nil {
				return nil, err
			}

		case keepkeyproto.MessageType_MessageType_ButtonRequest:
			// did we get a button request
			keepKey.ButtonAck()
			blocking = true

		case keepkeyproto.MessageType_MessageType_MessageSignature:
			//did we get a message signature
			var response = &keepkeyproto.MessageSignature{}
			keepKey.processResponse(rawResponse, response)
			done = true
			signature := &Signature{
				Address:   response.GetAddress(),
				Signature: response.GetSignature(),
				Message:   message,
			}

			//notify the listner functions
			if notifyPinFunc != nil && !notified {
				notifyPinFunc(false)
			}
			if responseFunc != nil {
				responseFunc(signature)
			}

			return signature, nil

		case keepkeyproto.MessageType_MessageType_Failure:
			// did we get a failure?
			response := &keepkeyproto.Failure{}
			keepKey.processResponse(rawResponse, response)
			log.Errorf("signmessage failure: %d %s", response.GetCode(), response.GetMessage())
			if responseFunc != nil {
				log.Infof("sending error response: %s", response.GetMessage())
				responseFunc(response.GetMessage())
			}

			return nil, errors.New(response.GetMessage())

		default:
			return nil, errors.New("Unknown message type for SignMessage")
		}
	}

	return nil, nil
}

// Initialize request that the device is Initialized to a start state.
// An error is returned if the device cannot be initialised.
func (keepKey *KeepKeyDevice) Initialize() error {
	log.Debug("initializing keepkey: ", keepKey.device.Path)
	request := &keepkeyproto.Initialize{}
	messageType := keepkeyproto.MessageType_value["MessageType_Initialize"]
	response := &keepkeyproto.Features{}

	err := keepKey.doRequestResponse(request, response, messageType)
	if err != nil {
		return err
	}

	keepKey.Features = response
	keepKey.DeviceID = response.GetDeviceId()
	log.Infof("keepkey device found '%s (%s)'", keepKey.Features.GetLabel(), keepKey.Features.GetDeviceId())
	return nil
}

// GetDevices will enumerate the USB KeepKey devices which are available on the
// system. An array of KeeyKeyDevice is returned for each valid device.
func GetDevices(pinAckFunc func(string) string) []*KeepKeyDevice {
	log.Debug("fetching keepkey devices")
	keepKeyDevices := []*KeepKeyDevice{}
	hidDevices := hid.Enumerate(vendorID, productID)
	log.Debug("number of keepkey devices found:", len(hidDevices))
	for _, newHidDevice := range hidDevices {

		// try to open the hid device
		openDevice, err := newHidDevice.Open()
		if err == nil {
			newKeepKeyDevice := &KeepKeyDevice{
				deviceInfo: newHidDevice,
				device:     openDevice,
				canCancel:  false,
			}
			newKeepKeyDevice.Initialize()
			newKeepKeyDevice.PinAck = pinAckFunc
			keepKeyDevices = append(keepKeyDevices, newKeepKeyDevice)
		} else {
			log.Error("keepkey device could not be opened: ", err.Error())
		}
	}
	return keepKeyDevices
}

// ConsolePinAck grab the pin from the console
func ConsolePinAck(deviceID string) string {
	pinChan := make(chan string)

	a := func() {
		fmt.Printf("Pin for '%s'> ", deviceID)
		reader := bufio.NewReader(os.Stdin)
		pin, _ := reader.ReadString('\n')
		pin = pin[:len(pin)-1]
		pinChan <- pin
	}
	go a()
	pin := <-pinChan
	return pin
}

// func main() {
// 	device := GetDevices(ConsolePinAck)[0]
//
// 	fmt.Println(device.GetFeatures())
// 	coin := device.Features.GetCoins()[1]
// 	fmt.Println(coin)
//
// 	path := []uint32{hardenedKey + 44, TestnetNetwork.Bip44Path, hardenedKey + 0}
// 	message := []byte("Hello World")
// 	signature, _ := device.SignMessage(path, message, TestnetNetwork)
// 	fmt.Printf("Address: %s\n", signature.Address)
// 	fmt.Printf("Message: %s\n", base64.StdEncoding.EncodeToString(message))
// 	fmt.Printf("Signature: %s\n", base64.StdEncoding.EncodeToString(signature.Signature))
//
// 	device.VerifyMessage(signature.Address, signature.Signature, message, TestnetNetwork)
// 	// entropy, _ := device.GetEntropy(32)
// 	// fmt.Println(entropy)
// 	// device.GetPublicKey(path)
// 	// device.GetAddress(path, "Testnet", true)
// }
