package main

import (
	"encoding/json"
	"io"
	"net/http"

	"bitbucket.org/titan098/go-btc/logging"
	"bitbucket.org/titan098/go-btc/settings"

	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

var log = logging.SetupLogging("server")

type responseMessage struct {
	Data  interface{} `json:"data"`
	Error bool        `json:"error"`
}

func sendError(w http.ResponseWriter, err error, status int) {
	response := responseMessage{
		Data:  err.Error(),
		Error: true,
	}
	jsonResponse, _ := json.Marshal(response)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	io.WriteString(w, string(jsonResponse))
}

func sendResponse(w http.ResponseWriter, response interface{}) {
	responseMsg := responseMessage{
		Data: response,
	}
	jsonResponse, _ := json.Marshal(responseMsg)
	w.Header().Set("Content-Type", "application/json")
	io.WriteString(w, string(jsonResponse))
}

func main() {
	deviceHandler := SetupDeviceHandler()
	accountHandler := SetupAccountHandler()
	defer accountHandler.Close()

	// Initialise the settings database
	settings.InitSettings()

	r := mux.NewRouter()

	// Routes for the device handlers
	r.HandleFunc("/device/list", deviceHandler.List).Methods("GET")
	r.HandleFunc("/device/entropy/{deviceid}/{size}", deviceHandler.Entropy).Methods("GET")
	r.HandleFunc("/device/publickey/{deviceid}/{coin}", deviceHandler.PublicKey).Methods("GET")
	r.HandleFunc("/device/getaddress/{deviceid}", deviceHandler.GetAddress).Methods("POST")
	r.HandleFunc("/device/masterpublickey/{deviceid}", deviceHandler.MasterPublicKey).Methods("GET")
	r.HandleFunc("/device/pin/{deviceid}", deviceHandler.Pin).Methods("POST")
	r.HandleFunc("/device/cancel/{deviceid}", deviceHandler.Cancel).Methods("GET")
	r.HandleFunc("/device/response/{deviceid}", deviceHandler.Response).Methods("GET")
	r.HandleFunc("/device/verifymessage/{deviceid}", deviceHandler.VerifyMessage).Methods("POST")
	r.HandleFunc("/device/signmessage/{deviceid}", deviceHandler.SignMessage).Methods("POST")
	r.HandleFunc("/device/signtransaction/{deviceid}", deviceHandler.SignTransaction).Methods("POST")

	//Routes for the account handlers
	r.HandleFunc("/account/init/{coin}/{xpub}", accountHandler.Init).Methods("GET")
	r.HandleFunc("/account/list", accountHandler.List).Methods("GET")
	r.HandleFunc("/account/get/{coin}/{xpub}", accountHandler.Get).Methods("GET")
	r.HandleFunc("/account/refresh/{coin}/{xpub}", accountHandler.Refresh).Methods("GET")
	r.HandleFunc("/account/refreshbalance/{coin}/{xpub}", accountHandler.RefreshBalance).Methods("GET")
	r.HandleFunc("/account/ledger/{coin}/{xpub}", accountHandler.Ledger).Methods("GET")
	r.HandleFunc("/account/unspent/{coin}/{xpub}", accountHandler.Unspent).Methods("GET")
	r.HandleFunc("/account/address/{coin}/{xpub}/{start}/{end}", accountHandler.Address).Methods("GET")
	r.HandleFunc("/account/receiveaddress/{coin}/{xpub}", accountHandler.ReceiveAddress).Methods("GET")
	r.HandleFunc("/account/path/{coin}/{xpub}/{address}", accountHandler.Path).Methods("GET")
	r.HandleFunc("/account/createtransaction/{coin}/{xpub}", accountHandler.CreateTransaction).Methods("POST")
	r.HandleFunc("/account/checkaddress/{coin}/{address}", accountHandler.CheckAddress).Methods("GET")
	r.HandleFunc("/account/estimatefee/{coin}/{xpub}", accountHandler.EstimateFee).Methods("POST")

	handler := cors.Default().Handler(r)

	srv := &http.Server{
		Handler: handler,
		Addr:    ":8000",
	}

	log.Info("listening...")
	log.Fatal(srv.ListenAndServe())
}
